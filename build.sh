#
#
# Modified by KLANG:technologies GmbH - 2014/06/17 'UBIFS header offset fix (2048)'
#
#
set -e

. /usr/local/oecore-i686/environment-setup-armv5te-angstrom-linux-gnueabi

make ARCH=arm CROSS_COMPILE=arm-angstrom-linux-gnueabi- clean
rm -f u-boot-ubl.bin
make ARCH=arm CROSS_COMPILE=arm-angstrom-linux-gnueabi- mityomapl138_config
make ARCH=arm CROSS_COMPILE=arm-angstrom-linux-gnueabi- -j3 #compile on 3 threads (faster compilation)

echo "Uploading it to NFS..."
cp u-boot-ubl.bin ~/klangnfs/fabian/u-boot-ubl.bin

md5sum  u-boot-ubl.bin
echo " = "
md5sum  ~/klangnfs/fabian/u-boot-ubl.bin
