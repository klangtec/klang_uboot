/*
 * fpga_spi.h
 *
 *  Created on: Sep 9, 2010
 *      Author: mikew
 */

#ifndef FPGA_SPI_H_
#define FPGA_SPI_H_

#define FPGA_SPI_TIMEOUT	(1*HZ)

#define MAX_FPGA_CHIP_SELECTS  8
#define MAX_FPGA_SPI_SPEED_HZ  250000000

/* Register Word (2 byte) Offsets */
static const int gnVER_OFFSET  = 0; // Standard Core Version Register
static const int gnCSR_OFFSET  = 1; // Control Register
static const int gnFWR_OFFSET  = 3; // Write FIFO Status Reg
static const int gnFDR_OFFSET  = 4; // FIFO Data Read/Write?
static const int gnFRR_OFFSET  = 6; // Read FIFO Status Reg
static const int gnIER_OFFSET  = 7; // Interurpt Enable Register
static const int gnIPR_OFFSET  = 8; // Interrupt Status Register
static const int gnDIV_OFFSET  = 9; // Divisor / chip select enable register
static const int gnDEL_OFFSET  = 10;// Delay Register

typedef union
{
    struct
    {
    	unsigned int rcv_en	: 1; /* 1 to enable receive */
    	unsigned int sclk_ctrl	: 1; /* 0 let clock free run, 1 = controlled clock */
    	unsigned int cpha	: 1; /* invert clock phase */
    	unsigned int cpol	: 1; /* invert clock polarity */
    	unsigned int dwidth	: 3; /* width = 4*(dwidth+1) */
    	unsigned int loopback	: 1; /* 1 to loop output back to input */
        unsigned int go         : 1; /* write 1 to start */
        unsigned int fifo_rst   : 1; /* write 1 to reset fifos */
    	unsigned int reserved	: 6;
    } msBits;
    unsigned short mnWord;
} tuSPICSR; // Control Register

typedef union
{
    struct
    {
        unsigned int write_cnt : 12; /* in words */
        unsigned int depth     : 3;  /* FIFO depth, 2^(depth+4) */
        unsigned int reserved  : 1;
    } msBits;
    unsigned short mnWord;
} tuSPIFWR; // Write FIFO Status.

typedef union
{
    struct
    {
        unsigned int mnData : 32;
    } ms32Bits;
    struct
    {
        unsigned int mnData : 24;
        unsigned int unused : 8;
    } ms24Bits;
    struct
    {
        unsigned int mnData : 16;
        unsigned int unused : 8;
    } ms16Bits;
    struct
    {
        unsigned int mnData : 8;
        unsigned int unused : 24;
    } ms8Bits;
    unsigned int mnWord;
} tuSPIFDR; // FIFO Data Read/Write

typedef union
{
	struct
	{
		unsigned int read_cnt : 12;  /* in words */
		unsigned int depth    : 3;   /* FIFO depth, 2^(depth+4) */
		unsigned int empty    : 1; /* FIFO empty */
	} msBits;
	unsigned short mnWord;
} tuSPIFRR; // Read FIFO Status.

typedef union
{
	struct
	{
		unsigned int mosi_tc	: 1; // SPI transfer complete interrupt enable.
		unsigned int rsv1	: 1; 
		unsigned int mosi_hf	: 1; // FIFO quarter half full interrupt enable.
		unsigned int rsv2	: 1; 
		unsigned int miso_nempty: 1; // 
		unsigned int rsv3	: 1; 
		unsigned int miso_hf	: 1; // FIFO quarter half full interrupt enable.
		unsigned int rsv4	: 1; 
		unsigned int rsv5	: 8; 
	} msBits;
	unsigned short mnWord;
} tuSPIIER; // Interrupt Enable Register

typedef union
{
	struct
	{
		unsigned int mosi_tc	: 1; // SPI transfer complete interrupt enable.
		unsigned int rsv1	: 1; 
		unsigned int mosi_hf	: 1; // FIFO quarter half full interrupt enable.
		unsigned int rsv2	: 1; 
		unsigned int miso_nempty: 1; // 
		unsigned int rsv3	: 1; 
		unsigned int miso_hf	: 1; // FIFO quarter half full interrupt enable.
		unsigned int rsv4	: 1; 
		unsigned int rsv5	: 8; 
	} msBits;
	unsigned short mnWord;
} tuSPIIPR; // Interrupt Status Register

typedef union 
{
	struct
	{
		unsigned int divisor : 12; // divisor clk = FREQ_EMIFA / (2*Divisor)
		unsigned int cs_en   : 3;  // chip select enable (0-7)
		unsigned int unused  : 1;
	} msBits;
	unsigned short mnWord;
} tuSPIDIV;

typedef union 
{
	struct
	{
		unsigned int delay : 15; // Delay count (# of SPI clocks between words)
		unsigned int delay_en : 1;// Delay enable
	} msBits;
	unsigned short mnWord;
} tuSPIDEL;


#endif /* FPGA_SPI_H_ */
