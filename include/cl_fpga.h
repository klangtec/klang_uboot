/**
 * \file cl_fpga.h
 * defines/declarations for CL FPGA core usage
 */
#ifndef CL_FPGA_H_
#define CL_FPGA_H_

/* These files are in the MDK.. must have a link in the u-boot include
 * path to MDK/include */
#include <fpga/fpga.h>
#include <fpga/core_ids.h>


/**
 * Generic FPGA device structure.  This represents a "core".
  */
struct fpga_device {
	struct coreversion      coreversion; /* the core version register information from FPGA */
	void*                   baseaddr;    /* the base address of the "core" */
	int                     coreindex;   /* the index of the core in the FPGA */
	int                     core_id;     /* the core ID of the core in the FPGA */
};
#define to_fpga_device(d) container_of(d, struct fpga_device, dev)

struct fpga_device* get_fpgadev(int core_id);
int read_core_version(void* baseaddr, struct coreversion* pdata);
const char* fpga_core_name(unsigned char ID);

#endif /* CONFIG_BLOCK_H_ */
