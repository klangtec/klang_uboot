/*
 * Copyright (C) 2010 Critical Link, LLC <www.criticallink.com>
 *
 * Copyright (C) 2008 Texas Instruments, Inc <www.ti.com>
 *
 * Based on da8530evm.h. Original Copyrights follow:
 *
 * Copyright (C) 2007 Sergey Kubushyn <ksi@koi8.net>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 * Modified by KLANG:technologies GmbH - 2014/06/17 'UBIFS header offset fix (2048'
 *
 */

#ifndef __CONFIG_H
#define __CONFIG_H

/*
 * General u-Boot config options
 */
#define CONFIG_ENV_OVERWRITE /* allow environment override of ethaddr */

/*
 * SoC Configuration
 */
#define CONFIG_MACH_MITYOMAPL138
#define CONFIG_ARM926EJS		/* arm926ejs CPU core */
#define CONFIG_SOC_DA8XX		/* TI DA8xx SoC */
#define CONFIG_SYS_CLK_FREQ		clk_get(DAVINCI_ARM_CLKID)
#define CONFIG_SYS_OSCIN_FREQ		24000000
#define CONFIG_SYS_TIMERBASE		DAVINCI_TIMER0_BASE
#define CONFIG_SYS_HZ_CLOCK		clk_get(DAVINCI_AUXCLK_CLKID)
#define CONFIG_SYS_HZ			1000
#define CONFIG_SKIP_LOWLEVEL_INIT
#define CONFIG_SKIP_RELOCATE_UBOOT	/* to a proper address, init done */

/*
 * Memory Info
 */
#define CONFIG_SYS_MALLOC_LEN	(0x10000 + 1*1024*1024) /* malloc() len */
#define CONFIG_SYS_GBL_DATA_SIZE	128 /* reserved for initial data */
#define PHYS_SDRAM_1		DAVINCI_DDR_EMIF_DATA_BASE /* DDR Start */
/* TODO - this define is only used to print out how much memory there is,
 * if we start building boards that have different memory options, we should
 * remove this define and probe it in the dram_init method that uses this field.
 */
#define PHYS_SDRAM_1_SIZE	(128 << 20) /* SDRAM size 128MB */
#define CONFIG_SYS_MEMTEST_START	PHYS_SDRAM_1 + 0x2000000 /* memtest start addr */
#define CONFIG_SYS_MEMTEST_END 	(PHYS_SDRAM_1 + 0x2000000 + 32*1024*1024) /* 32MB test */
#define CONFIG_NR_DRAM_BANKS	1 /* we have 1 bank of DRAM */
#define CONFIG_STACKSIZE	(256*1024) /* regular stack */

/*
 * Serial Driver info
 */
#define CONFIG_SYS_NS16550
#define CONFIG_SYS_NS16550_SERIAL
#define CONFIG_SYS_NS16550_REG_SIZE	-4	/* NS16550 register size */
#define CONFIG_SYS_NS16550_COM1	DAVINCI_UART1_BASE /* Base address of UART1 */
#define CONFIG_SYS_NS16550_CLK	clk_get(DAVINCI_UART1_CLKID)
#define CONFIG_CONS_INDEX	1		/* use for console */
#define CONFIG_BAUDRATE		115200		/* Default baud rate */
#define CONFIG_SYS_BAUDRATE_TABLE	{ 9600, 19200, 38400, 57600,\
					  115200, 230400, 460800, 921600 }

#define CONFIG_SPI
#define CONFIG_SPI_FLASH
#define CONFIG_SPI_FLASH_STMICRO
#define CONFIG_DAVINCI_SPI
#define CONFIG_SYS_SPI_BASE		DAVINCI_SPI1_BASE
#define CONFIG_SYS_SPI_CLK		clk_get(DAVINCI_SPI1_CLKID)
#define CONFIG_SF_DEFAULT_SPEED		30000000
#define CONFIG_ENV_SPI_MAX_HZ		CONFIG_SF_DEFAULT_SPEED
#define CONFIG_MAX_SPI_BYTES 128
#define CONFIG_FPGA
#define CONFIG_NUM_SPI_BUSES 2 /* On-board and FPGA */
#define CONFIG_FPGA_SPI
#define CONFIG_FPGA_BASE_ADDR 0x66000000 /* Base address of FPGA cores. */
#define CONFIG_FPGA_CORE_OFFSET 0x80 /* Offset between cores. */
#define CONFIG_FPGA_NUM_CORES 16 /* Max number of FPGA cores. */

/*
 * I2C Configuration
 */
#define CONFIG_HARD_I2C
#define CONFIG_DRIVER_DAVINCI_I2C
#define CONFIG_SYS_I2C_SPEED		25000 /* 100Kbps won't work, H/W bug */
#define CONFIG_SYS_I2C_SLAVE		10 /* Bogus, master-only in U-Boot */

/*
 * I2C EEPROM definitions for catalyst 24W256 EEPROM chip
 */
#define CONFIG_SYS_I2C_EEPROM_ADDR_LEN	2
#define CONFIG_SYS_I2C_EEPROM_ADDR	0x50
#define CONFIG_SYS_I2C_EXPANDER_ADDR	0x20
#define CONFIG_SYS_EEPROM_PAGE_WRITE_BITS	6
#define CONFIG_SYS_EEPROM_PAGE_WRITE_DELAY_MS	20

/*
 * Network & Ethernet Configuration
 */
#define	CONFIG_DRIVER_TI_EMAC
#ifdef CONFIG_DRIVER_TI_EMAC
#define CONFIG_MII
#define CONFIG_BOOTP_DEFAULT
#define CONFIG_BOOTP_DNS
#define CONFIG_BOOTP_DNS2
#define CONFIG_BOOTP_SEND_HOSTNAME
#define CONFIG_NET_RETRY_COUNT	10
#define CONFIG_NET_MULTI
#define CONFIG_DRIVER_TI_EMAC_USE_RMII
#endif

/*
 * NAND Flash
 */
#define CONFIG_NAND_DAVINCI
#define	CONFIG_SYS_NAND_USE_FLASH_BBT
#define CONFIG_SYS_NAND_4BIT_HW_ECC_OOBFIRST /* 16 bit NAND does not support 4 BIT ECC */
#define	CONFIG_SYS_NAND_PAGE_2K
#define CONFIG_SYS_NAND_CS		3 // [fabian]: TODO OPT 3->2
#define CONFIG_SYS_NAND_BASE		DAVINCI_ASYNC_EMIF_DATA_CE3_BASE
#define CONFIG_SYS_CLE_MASK		0x10
#define CONFIG_SYS_ALE_MASK		0x8
#define CONFIG_SYS_NAND_HW_ECC              /* turn on 1 bit HW ECC support (for 16 bit devices) */
#define CONFIG_SYS_MAX_NAND_DEVICE	1 /* Max number of NAND devices */
#define NAND_MAX_CHIPS			1
#define CONFIG_SYS_NAND_QUIET_TEST
#define CONFIG_MTD_DEVICE
#define CONFIG_MTD_PARTITIONS
#undef CONFIG_MTD_DEBUG
#ifdef CONFIG_MTD_DEBUG
#define CONFIG_MTD_DEBUG_VERBOSE	4
#endif
#define CONFIG_JFFS2_NAND 1 // [fabian]: TODO OPT remove
#define CONFIG_ENV_IS_IN_SPI_FLASH
#define CONFIG_ENV_SIZE			(64 << 10) // [fabian]: TODO OPT smaller
#define CONFIG_ENV_OFFSET		(576 << 10)
#define CONFIG_MITYDSP_ENV_SIZE 	(64 << 10) // [fabian]: TODO OPT smaller
#define CONFIG_MITYDSP_ENV_OFFSET ((576+64) << 10)
#define CONFIG_ENV_SECT_SIZE	(64 << 10)
#define CONFIG_SYS_NO_FLASH

/*
 * MMC configuration
 */
#define CONFIG_MMC
#define CONFIG_GENERIC_MMC
#define CONFIG_DAVINCI_MMC
#define CONFIG_MMC_MBLOCK

#define CONFIG_DOS_PARTITION
#define CONFIG_CMD_EXT2
#define CONFIG_CMD_FAT
#define CONFIG_CMD_MMC
#undef CONFIG_ENV_IS_IN_MMC

/*
 * USB  configuration
 */
// [fabian]: TODO opt remove USB
#define CONFIG_USB_DA8XX      /* Platform hookup to MUSB controller */
#define CONFIG_MUSB_HCD

/*
 * U-Boot general configuration
 */
#undef CONFIG_USE_IRQ			/* No IRQ/FIQ in U-Boot */
#define CONFIG_MISC_INIT_R
#define CONFIG_BOOTFILE		"uImage" /* Boot file name */
#define CONFIG_SYS_PROMPT	"U-Boot > " /* Command Prompt */
#define CONFIG_SYS_CBSIZE	1024 /* Console I/O Buffer Size	*/
#define CONFIG_SYS_PBSIZE	(CONFIG_SYS_CBSIZE+sizeof(CONFIG_SYS_PROMPT)+16)
#define CONFIG_SYS_MAXARGS	16 /* max number of command args */
#define CONFIG_SYS_BARGSIZE	CONFIG_SYS_CBSIZE /* Boot Args Buffer Size */
#define CONFIG_SYS_LOAD_ADDR	(PHYS_SDRAM_1 + 0x700000)
#define CONFIG_VERSION_VARIABLE
#define CONFIG_AUTO_COMPLETE	/* Won't work with hush so far, may be later */
#undef CONFIG_SYS_HUSH_PARSER // [fabian]: OPTIMIZATION
#define CONFIG_SYS_PROMPT_HUSH_PS2	"> "
#define CONFIG_CMDLINE_EDITING
#undef CONFIG_SYS_LONGHELP // [fabian]: OPTIMIZATION
#undef CONFIG_CRC32_VERIFY  /* [fabian]: OPTIMIZATION */
#define CONFIG_MX_CYCLIC
#define CONFIG_CMD_ELF

/*
 * Linux Information
 */
#define LINUX_BOOT_PARAM_ADDR	(PHYS_SDRAM_1 + 0x100)
#define CONFIG_CMDLINE_TAG
#define CONFIG_SERIAL_TAG
#define CONFIG_PERIPHERALS_TAG
#define CONFIG_SETUP_MEMORY_TAGS
#define CONFIG_BOOTARGS		"mem=96M console=ttyS1,115200n8 root=/dev/mtdblock0 rw rootwait"
#define CONFIG_BOOTCOMMAND	"sf probe 0;sf read 0xc0700000 0x100000 0x280000;bootm 0xc0700000"
#define CONFIG_BOOTDELAY	1  // [fabian] OPTIMIZATION (3->1)

#define CONFIG_EXTRA_ENV_SETTINGS 
/* // [fabian]: removed
#define CONFIG_EXTRA_ENV_SETTINGS \
		"flashuboot=tftp 0xc0700000 mityomap/u-boot-ubl.bin; sf probe 0; " \
			"sf erase 0x10000 0x80000; sf write 0xc0700000 0x10000 ${filesize}\0" \
		"flashkernel=tftp 0xc0700000 mityomap/uImage; sf probe 0; " \
			"sf erase 0x100000 0x280000; sf write 0xc0700000 0x100000 ${filesize}\0" \
		"flashubl=tftp 0xc0700000 mityomap/UBL_SPI_MEM.ais; sf probe 0; " \
			"sf erase 0 0x10000; sf write 0xc0700000 0 0x10000\0" \
		"flashrootfs=tftp 0xc2000000 mityomap/mityomap-base-mityomapl138.jffs2; " \
		    "nand erase 0 0x08000000; nand write.jffs2 0xc2000000 0 ${filesize}\0" \
		"serverip=10.0.0.23\0" \
		"autoload=no\0" \
		"mtdids=nand0=nand\0" \
		"mtdparts=mtdparts=nand:128M(rootfs),-(userfs)\0" \
		"bootargsbase=mem=96M console=ttyS1,115200n8\0" \
		"flashargs=setenv bootargs ${bootargsbase} ${mtdparts} " \
			"root=/dev/mtdblock0 rw,noatime rootfstype=jffs2\0"
			*/
/*
 * U-Boot commands
 */
#include <config_cmd_default.h>
#define CONFIG_CMD_ENV
#define CONFIG_CMD_ASKENV
#define CONFIG_CMD_DHCP
#define CONFIG_CMD_DIAG
#define CONFIG_CMD_MII
#define CONFIG_CMD_PING
#define CONFIG_CMD_SAVES
#define CONFIG_CMD_MEMORY
#define CONFIG_CMD_NAND
#define CONFIG_CMD_SPI
#define CONFIG_CMD_SF
#define CONFIG_CMD_SAVEENV
#define CONFIG_CMD_JFFS2
#define CONFIG_CMD_MTDPARTS
#define CONFIG_CMD_I2C

// [fabian] TODO OPT: remove USB?
#ifdef CONFIG_USB_DA8XX

	#ifdef CONFIG_MUSB_HCD
		#define CONFIG_CMD_USB		/* inclue support for usb       */

		#define CONFIG_USB_STORAGE	/* MSC class support */
		#define CONFIG_CMD_STORAGE	/* inclue support for usb       */
		#define CONFIG_CMD_FAT		/* inclue support for FAT/storage*/ 
		#define CONFIG_DOS_PARTITION	/* inclue support for FAT/storage*/

		#ifdef CONFIG_USB_KEYBOARD
			#define CONFIG_SYS_USB_EVENT_POLL
			#define CONFIG_PREBOOT "usb start"
		#endif /* CONFIG_USB_KEYBOARD */

	#endif /* CONFIG_MUSB_HCD */

	#ifdef CONFIG_MUSB_UDC
		/* USB device configuration */
		#define CONFIG_USB_DEVICE		1
		#define CONFIG_USB_TTY			1
		#define CONFIG_SYS_CONSOLE_IS_IN_ENV	1
		/* Change these to suit your needs */
		#define CONFIG_USBD_VENDORID		0x0451
		#define CONFIG_USBD_PRODUCTID		0x5678
		#define CONFIG_USBD_MANUFACTURER	"Critical Link LLC"
		#define CONFIG_USBD_PRODUCT_NAME	"MityDSP-L138"
	#endif /* CONFIG_MUSB_UDC */

#endif /* CONFIG_USB_DA8XX */

#define CONFIG_UBL_IMAGE

/* [fabian] UBIFS support */
#define CONFIG_CMD_UBI
#define CONFIG_CMD_UBIFS
#define CONFIG_RBTREE
#define CONFIG_LZO

#endif /* __CONFIG_H */
