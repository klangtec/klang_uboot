/*
 * (C) Copyright 2002-2008
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 *
 * Modified by KLANG:technologies GmbH - 2016/06/17 'Uboot-UBIfs NAND-ECC-Error korrigiert'
 * Modified by KLANG:technologies GmbH - 2018/01/02 'Default-env-variables are now also in fw_set/printenv.'
 *
 */

/*
 * To build the utility with the static configuration
 * comment out the next line.
 * See included "fw_env.config" sample file
 * for notes on configuration.
 */
#define CONFIG_FILE     "/etc/fw_env.config"

//#define HAVE_REDUND /* For systems with 2 env sectors */
#define DEVICE1_NAME      "/dev/mtd4"
#define DEVICE2_NAME      "/dev/mtd4"
#define DEVICE1_OFFSET    0x0000
#define ENV1_SIZE         0x10000
#define DEVICE1_ESIZE     0x10000
#define DEVICE1_ENVSECTORS     2
#define DEVICE2_OFFSET    0x0000
#define ENV2_SIZE         0x10000
#define DEVICE2_ESIZE     0x10000
#define DEVICE2_ENVSECTORS     2

#define CONFIG_BAUDRATE		115200

// BKR: MAKE SURE THAT THESE SETTINGS ARE THE SAME AS IN INCLUDE/CONFIGS/MITYOMAPL138.h!!!!! VERY IMPORTANT!
#define CONFIG_EXTRA_ENV_SETTINGS \
		"readtouch=if itest ${triedusb} -ne 1; then if itest *0x01e260c0 -eq 0x0000efff; " \
			"then run flashdisplay; run usbboot; else echo notouch; fi; else echo alreadytried; fi;\0" \		
		"autoload=no\0" \
		"mtdids=nand0=nand\0" \
		"mtdparts=mtdparts=nand:128M(rootfs),-(userfs)\0" \
		"bootargsbase=mem=96M console=ttyS1,115200n8\0" \
		"checktouch=setenv triedusb 0; run readtouch; run readtouch; run readtouch; " \
			"run readtouch; run readtouch; run readtouch; run readtouch; run readtouch; " \
			"run readtouch; run readtouch; run readtouch; run readtouch; run readtouch; run networktry;\0" \
		"usbboot=setenv triedusb 1; usb start; usb start; fatload usb 0:1 0xc0700000 KLANG.recovery; " \
			"source 0xc0700000;\0" \		
		"flashdisonce=i2c mw 0x36 0x03 0x20; msleep 500; i2c mw 0x36 0x03 0xff; msleep 500;\0" \
		"flashdisplay=run flashdisonce; run flashdisonce; run flashdisonce; run flashdisonce;\0" \
		"netretry=no\0" \
		"networktry=if itest ${triedusb} -eq 1; then dhcp; if ping 192.168.0.200; then run networkupdate; " \
		"else echo updateserver; fi; else echo notouchnonetwork; fi;\0" \
		"networkupdate=tftp 0xc0700000 192.168.0.200:KLANG.recovery; source 0xc0700000;\0" \
		"serverip=10.0.0.23\0" \		
		"bootdefault=ubi part rootfs; run bootdsp; ubi read 0xc0007fc0 bootfs 0x300000; bootm 0xc0007fc0;\0" \
		"KLANG_serial=modulnamestr\0" \
		"displayon=i2c mw 0x36 0x50 0x03; i2c mw 0x36 0x01 0x08; i2c mw 0x36 0x07 0x12; i2c mw 0x36 0x02 0x49; i2c mw 0x36 0x05 0x14; i2c mw 0x36 0x00 0x04; i2c mw 0x36 0x03 0xff; i2c mw 0x27 0x00 0x00; i2c mw 0x27 0x01 0x2f; i2c mw 0x27 0x0c 0xff; i2c mw 0x27 0x0b 0x1f; i2c mw 0x27 0x03 0xff; i2c mw 0x27 0x04 0x00; i2c mw 0x27 0x05 0x00;\0" \
		"bootdsp=sf probe 0; sf read 0xc0700000 0x400000 0x200000; bootdsp 0xc0700000;\0" \
		"baudrate=115200\0" \
		"Mi_PartNr=L138-FX-225-RC\0" \		
		"autoboot=no\0" \
		"bootfile=uImage\0" \
		"stdin=serial\0" \
		"stdout=serial\0" \
		"stderr=serial\0" \
		"partition=nand0,0\0" \
		"mtddevnum=0\0" \
		"serverip=10.0.0.23\0" \
		"ipaddr=10.0.0.25\0" \
		"mtddevname=rootfs\0"

/*#define CONFIG_EXTRA_ENV_SETTINGS							\
"KLANG_last_update=USB_RECOVERY\0" \
"bootdsp=sf probe 0; sf read 0xc0700000 0x400000 0x200000; bootdsp 0xc0700000;\0" \
"displayon=i2c mw 0x36 0x50 0x03; i2c mw 0x36 0x01 0x08; i2c mw 0x36 0x07 0x12; i2c mw 0x36 0x02 0x49; i2c mw 0x36 0x05 0x14; i2c mw 0x36 0x00 0x04; i2c mw 0x36 0x03 0xff;\0" \
"KLANG_serial=KFZNOSER\0" \
"bootusb=setenv bootselect default; saveenv; usb start; fatload usb 0:1 0xc4000000 KLANGupdate.img; source 0xc4000000; reset;\0" \
"bootdefault=ubi part rootfs; run bootdsp; ubi read 0xc0007fc0 bootfs 0x300000; bootm 0xc0007fc0;\0" \
"bootdelay=1\0" \
"readtouch=if itest ${triedusb} -ne 1; then if itest *0x01e260c0 -eq 0x0000efff; then run flashdisplay; run usbboot; else echo notouch; fi; else echo alreadytried; fi;\0" \
"checktouch=setenv triedusb 0; run readtouch; run readtouch; run readtouch; run readtouch; run readtouch; run readtouch; run readtouch; run readtouch; run readtouch; run readtouch; run readtouch; run readtouch; run readtouch; run networktry\0" \
"usbboot=setenv triedusb 1; usb start; usb start; fatload usb 0:1 0xc0700000 KLANG.recovery; source 0xc0700000;\0" \
"bootcmd=run displayon; ubi part rootfs; run checktouch; run bootdefault;\0" \
"flashdisonce=i2c mw 0x36 0x03 0x20; msleep 500; i2c mw 0x36 0x03 0xff; msleep 500;\0" \
"flashdisplay=run flashdisonce; run flashdisonce; run flashdisonce; run flashdisonce;\0" \
"netretry=no\0" \
"networktry=if itest ${triedusb} -eq 1; then dhcp; if ping 192.168.0.200; then run networkupdate; else echo updateserver; fi; else echo notouchnonetwork; fi;\0" \
"networkupdate=tftp 0xc0700000 192.168.0.200:KLANG.recovery; source 0xc0700000;\0" \
"autoboot=no\0" \
"autoload=no\0" \
"mtdids=nand0=nand\0" \
"mtdparts=mtdparts=nand:128M(rootfs),-(userfs)\0" \
"bootargsbase=mem=96M console=ttyS1,115200n8\0" \
"partition=nand0,0\0" \
"mtddevnum=0\0" \
"mtddevname=rootfs\0" \
"bootargs=mem=96M console=ttyS1,115200n8 mtdparts=nand:128M(rootfs),-(userfs) root=ubi0:rootfs ro ubi.mtd=0,2048 ubi.fm_autoconvert=1 rootfstype=ubifs ip=off\0" \
"bootfile=uImage\0" \
"gatewayip=192.168.188.1\0" \
"netmask=255.255.255.0\0" \
"ipaddr=192.168.188.85\0" \
"serverip=192.168.188.1\0" \
"dnsip=137.226.61.21\0" \
"dnsip2=134.130.4.1\0"*/


extern int   fw_printenv(int argc, char *argv[]);
extern char *fw_getenv  (char *name);
extern int fw_setenv  (int argc, char *argv[]);
extern int fw_parse_script(char *fname);
extern int fw_env_open(void);
extern int fw_env_write(char *name, char *value);
extern int fw_env_close(void);

extern unsigned	long  crc32	 (unsigned long, const unsigned char *, unsigned);
