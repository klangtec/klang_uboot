/*
 * genublimg.c
 *
 *  Created on: Apr 12, 2010
 *      Author: mitydspl138
 */

#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define UBL_MAGIC_BINARY_BOOT       (0x55424CBBu)

struct UBLHeader
{
	uint32_t magicNum;
	uint32_t entryPoint;
	uint32_t appSize;
	uint32_t memAddress;
	uint32_t ldAddress;
};

struct UBLHeader UBLHeader = {
		.magicNum = UBL_MAGIC_BINARY_BOOT,
		.entryPoint = 0xC1080000,
		.appSize = (512 << 10),
		.memAddress = 0x10000 + sizeof(UBLHeader),
		.ldAddress = 0xC1080000
};

void usage(void)
{
	fprintf (stderr, "Usage: genublimage -i image.bin -o image_ubl.bin\n");
	exit (-1);
}

int main(int argc, char* argv[])
{
	char* infile_name = NULL;
	char* outfile_name = NULL;
	FILE* infile;
	FILE* outfile;
	int   size;
	char  buffer[1024];
	int   writesize;
	int   bytes_written = 0;

	while (--argc > 0 && **++argv == '-') {
		while (*++*argv) {
			switch (**argv) {
			case 'i':
				if (--argc <= 0)
					usage ();
				infile_name = *++argv;
				goto NXTARG;
			case 'o':
				if (--argc <= 0)
					usage ();
				outfile_name = *++argv;
				goto NXTARG;
			default:
				usage();
			}
		}
NXTARG:		;
	}

	if (!infile_name || !outfile_name)
	{
		usage();
	}

	if (NULL == (infile = fopen(infile_name,"rb")))
	{
		fprintf(stderr, "Unable to open %s for reading\n", infile_name);
		exit(-1);
	}

	if (NULL == (outfile = fopen(outfile_name,"wb")))
	{
		fclose(infile);
		fprintf(stderr, "Unable to open %s for writing\n", outfile_name);
		exit(-1);
	}

	fseek(infile, 0, SEEK_END);
	size = ftell(infile);
	UBLHeader.appSize = size;
	fseek(infile, 0, SEEK_SET);
	fprintf(stdout, "Writing %d byte image with UBL header\n", size);

	fwrite(&UBLHeader, 1, sizeof(UBLHeader), outfile);
	while(bytes_written < size)
	{
		writesize = (size-bytes_written < 1024) ? (size-bytes_written) : 1024;
		if (writesize != fread(buffer, 1, writesize, infile))
		{
			fprintf(stderr, "Unable to read %d bytes from input file\n", writesize);
		}
		if (writesize != fwrite(buffer, 1, writesize, outfile))
		{
			fprintf(stderr, "Unable to write %d bytes to output file\n", writesize);
		}
		bytes_written += writesize;
	}

	fclose(infile);
	fclose(outfile);
	return 0;
}
