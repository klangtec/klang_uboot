/*
 * Copyright (C) 2010 Critical Link, LLC <www.criticallink.com>
 *
 * Copyright (C) 2008 Texas Instruments, Inc <www.ti.com>
 *
 * Based on spi.c. Original Copyrights follow:
 *
 * (C) Copyright 2002
 * Gerald Van Baren, Custom IDEAS, vanbaren@cideas.com
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */

/*
 * FPGA SPI Core Read/Write Utilities
 */

#include <common.h>
#include <spi.h>
#include <malloc.h>
#include <asm/io.h>
#include <asm/arch/hardware.h>
#include <cl_fpga.h>
#include <fpga/fpga_spi.h>

static struct fpga_device* fpgadev = NULL;
struct fpga_spi_slave {
	struct spi_slave slave;
	struct fpga_device*	fpgadev;
	uint bytes_per_word;
};

static inline struct fpga_spi_slave *to_fpga_spi(struct spi_slave *slave)
{
        return container_of(slave, struct fpga_spi_slave, slave);
}

/* Davinci SPI funcs. This file provides the uboot SPI interface and calls
   into the davinci or fpga versions accordingly.
 */
void davinci_spi_init(void);
struct spi_slave *davinci_spi_setup_slave(unsigned int bus, unsigned int cs,
					unsigned int max_hz, unsigned int mode);
void davinci_spi_free_slave(struct spi_slave *slave);
int davinci_spi_claim_bus(struct spi_slave *slave);
void davinci_spi_release_bus(struct spi_slave *slave);
int davinci_spi_xfer(struct spi_slave *slave, unsigned int bitlen,const void *dout,
			void *din, unsigned long flags);
int davinci_spi_cs_is_valid(unsigned int bus, unsigned int cs);
void davinci_spi_cs_activate(struct spi_slave *slave);
void davinci_spi_cs_deactivate(struct spi_slave *slave);

/*-----------------------------------------------------------------------
 * Definitions
 */

#define MAX_XFER_BYTES (4) //!< Maximum number of bytes we 
	//!< can handle. We will not make assumptions on FIFO sizes and only 
	//!< perform up to a single 32-bit SPI transfer. 



static void fpga_spi_init(void)
{
	if(!fpgadev) {
		fpgadev = get_fpgadev(CORE_ID_SPI);
		if(fpgadev) {
			unsigned short* lpBaseAddr = fpgadev->baseaddr;
			tuSPICSR CSR;
			/* reset fifos */
			CSR.mnWord = lpBaseAddr[gnCSR_OFFSET];
			CSR.msBits.fifo_rst = 1;
			lpBaseAddr[gnCSR_OFFSET] = CSR.mnWord;
		}
	}
	if(!fpgadev)
		printf("ERROR!!! FPGA Device not found!\n");
}

static struct spi_slave *fpga_spi_setup_slave(unsigned int bus, unsigned int cs,
					  unsigned int max_hz, unsigned int mode)
{
	struct fpga_spi_slave	*ds;

	int divisor;
	tuSPICSR CSR;
	tuSPIDIV DIV;
	int emifa_rate = 100000000;
	unsigned short* lpBaseAddr;
	
	/* Delayed call as FPGA is not loaded when noral SPI is initialized */
	fpga_spi_init();

	/*printf("%s: bus = %d device = %d f = %d m = %d\n",__FUNCTION__, bus, cs, max_hz, mode); */
	if (!spi_cs_is_valid(bus, cs))
		return NULL;

	ds = malloc(sizeof(*ds));
	if (!ds)
		return NULL;

	ds->slave.bus = bus;
	ds->slave.cs = cs;
	ds->fpgadev=fpgadev;
	lpBaseAddr = fpgadev->baseaddr;
	CSR.mnWord = lpBaseAddr[gnCSR_OFFSET];
	DIV.mnWord = lpBaseAddr[gnDIV_OFFSET];
	lpBaseAddr[gnIER_OFFSET] = 0; /* disable interrupts */

	/* we do not support LSB FIRST shifting */
	if (mode & SPI_LSB_FIRST)
		return NULL;

	CSR.msBits.cpol = (mode & SPI_CPOL) ? 1 : 0;
	CSR.msBits.cpha = (mode & SPI_CPHA) ? 1 : 0;
	CSR.msBits.loopback = (mode & SPI_LOOP) ? 1 : 0;
	/* Hard coded 8 bit SPI word.. If you change CSR dwidth you must change ds->bytes_per_word also*/
	CSR.msBits.dwidth = 3; /*8 bit word */
	ds->bytes_per_word = 2;

	/* There's no SPI framework mode for this -- default to SYNC-gated,
	 * because that's what most SPI devices do */
	CSR.msBits.sclk_ctrl = 1;

        /* Set rcv_en if there is a receive buffer requested */
        CSR.msBits.rcv_en = 1;

	/* loopback */
	CSR.msBits.loopback = (mode & SPI_LOOP) ? 1 : 0;

#if 0
	Comment out as SPI_NO_CS is not defined
	/* chip select */
	if (!(mode & SPI_NO_CS)) {
		DIV.msBits.cs_en = cs;
	}
#else
	DIV.msBits.cs_en = cs;
#endif /* 0 */
	
	
	divisor = emifa_rate / (2 * max_hz);
	if (divisor == 0)
		divisor = 1;
	if ((emifa_rate / (2 * divisor)) > max_hz)
		divisor += 1;
	if (divisor > 0x0FFF)
		divisor = 0x0FFF;
	DIV.msBits.divisor = divisor;
	lpBaseAddr[gnDIV_OFFSET] = DIV.mnWord;
	lpBaseAddr[gnCSR_OFFSET] = CSR.mnWord;
	return &ds->slave;
}

static void fpga_spi_free_slave(struct spi_slave *slave)
{
	struct fpga_spi_slave *ds = to_fpga_spi(slave);

	free(ds);
}


static int fpga_spi_claim_bus(struct spi_slave *slave)
{
	return fpgadev?0:-1;

}

static void fpga_spi_release_bus(struct spi_slave *slave)
{
#if 0	/* Don't do this */
	if(fpgadev) {
		unsigned short* lpBaseAddr = fpgadev->baseaddr;
		tuSPICSR CSR;
		/* reset fifos */
        CSR.mnWord = lpBaseAddr[gnCSR_OFFSET];
		CSR.msBits.fifo_rst = 1;
		CSR.msBits.rcv_en = 0;
        lpBaseAddr[gnCSR_OFFSET] = CSR.mnWord;
	}
#endif /* 0 */
}

/*
 * SPI read/write
 *
 * Syntax:
 *   fpgaspi {dev} {num_bits} {dout}
 *	fpgaspi <core_num> <cs> <bit_len> <dout> - 
 *	Send <bit_len> bits from <dout> out the FPGA SPI core number <core_num>
 *		using chip select <cs>
 *	<core_num> - The FPGA core number
 *	<cs>       - Identifies the chip select of the device
 *	<bit_len>  - Number of bits to send (base 10)
 *	<dout>     - Hexadecimal string that gets sent
 * The command prints out the hexadecimal string received via SPI.
 */
static int fpga_spi_xfer(struct spi_slave *slave, unsigned int bitlen,
		const void *dout, void *din, unsigned long flags)
{
	struct fpga_spi_slave *ds = to_fpga_spi(slave);
	int	len;
	/*
	unsigned int	data1_reg_val = readl(&ds->regs->dat1);
	*/
	int		ret;
	const u8	*txp = dout; /* dout can be NULL for read operation */
	unsigned short* lpBaseAddr = fpgadev->baseaddr;
	tuSPIFRR FRR;
	tuSPIFDR FDR;
	tuSPICSR CSR;
	tuSPIFWR FWR;
	int ii;
	int bpw = ds->bytes_per_word;

	ret = 0;

	if (bitlen == 0)
		/* Finish any previously submitted transfers */
		goto out;

	/*
	 * It's not clear how non-8-bit-aligned transfers are supposed to be
	 * represented as a stream of bytes...this is a limitation of
	 * the current SPI interface - here we terminate on receiving such a
	 * transfer request.
	 */
	if (bitlen % 8) {
		/* Errors always terminate an ongoing transfer */
		flags |= SPI_XFER_END;
		goto out;
	}
	/* printf("%s: using SPI core @ %08x\n",__FUNCTION__, (uint32_t)lpBaseAddr); */

#if 0
	len=100000000;
	do {
		unsigned short temp = lpBaseAddr[gnFDR_OFFSET];
		FRR.mnWord = lpBaseAddr[gnFRR_OFFSET];
		--len;
	} while(len || !FRR.msBits.empty);
	if(!len)
		printf("%s: ERROR - timeout waiting for FIFO clear completion\n",__FUNCTION__);
#endif

	/* printf("%s: CSR =  %04x\n",__FUNCTION__, lpBaseAddr[gnCSR_OFFSET]); */

	FWR.mnWord = lpBaseAddr[gnFWR_OFFSET];
	len = bitlen / 8;
	printf("%s:xfering %d bytes bpw = %d\n", __FUNCTION__, len, bpw);
	do {
		FDR.mnWord = 0;
		for (ii = 0; ii < bpw; ++ii)
			FDR.mnWord |= (unsigned) *txp++ << (ii * 8);
		lpBaseAddr[gnFDR_OFFSET] = FDR.mnWord;
		lpBaseAddr[gnFDR_OFFSET+1] = 0; /* need upper 16 bits written to force FIFO update */
		/* printf(" %08x = %04x\n", &lpBaseAddr[gnFDR_OFFSET], FDR.mnWord); */
		len -= bpw;
	} while (len>0);
	/* start the outbound transfer by setting go bit */
       	CSR.mnWord = lpBaseAddr[gnCSR_OFFSET];
       	CSR.msBits.go = 1;
       	CSR.msBits.rcv_en = 1; /* no receive yet */
       	lpBaseAddr[gnCSR_OFFSET] = CSR.mnWord;

	len=100000000;
	while(len > 0) {
		len = (lpBaseAddr[gnIPR_OFFSET] & 1)?0:len-1; /* wait for transfer complete */
	}
	if(!(lpBaseAddr[gnIPR_OFFSET] & 1)) {
		printf("%s: ERROR - timeout waiting for completion",__FUNCTION__);
	}
out:
	return 0;
}
 

/* Here's the u-boot interface funcs... */

void spi_init(void)
{
	/* Initialize both the on-board SPI controller and the FPGA SPI core */
	davinci_spi_init();
	/* don't call fpga_spi_init() yet as fpga is not loaded */
}

struct spi_slave *spi_setup_slave(unsigned int bus, unsigned int cs,
					  unsigned int max_hz, unsigned int mode)
{
	if(1 == bus)
		return fpga_spi_setup_slave(bus, cs, max_hz, mode);
	else
		return davinci_spi_setup_slave(bus, cs, max_hz, mode);
}

void spi_free_slave(struct spi_slave *slave)
{
	if(1 == slave->bus)
		return fpga_spi_free_slave(slave);
	else
		return davinci_spi_free_slave(slave);
}

int spi_claim_bus(struct spi_slave *slave)
{
	if(1 == slave->bus)
		return fpga_spi_claim_bus(slave);
	else
		return davinci_spi_claim_bus(slave);
}

void spi_release_bus(struct spi_slave *slave)
{
	if(1 == slave->bus)
		return fpga_spi_release_bus(slave);
	else
		return davinci_spi_release_bus(slave);
}

int spi_xfer(struct spi_slave *slave, unsigned int bitlen,
		const void *dout, void *din, unsigned long flags)
{
	if(1 == slave->bus)
		return fpga_spi_xfer(slave, bitlen, dout, din, flags);
	else
		return davinci_spi_xfer(slave, bitlen, dout, din, flags);
}

int spi_cs_is_valid(unsigned int bus, unsigned int cs)
{
	if(1 == bus) {
		/* FPGA SPI is bus 1 */
		return (fpgadev?1:0);
	} else {
		return davinci_spi_cs_is_valid(bus, cs);
	}
}

void spi_cs_activate(struct spi_slave *slave)
{
	/* do nothing */
}

void spi_cs_deactivate(struct spi_slave *slave)
{
	/* do nothing */
}

