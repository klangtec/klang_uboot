/*
 * Control GPIO pins on the fly
 *
 * Copyright (c) 2008 Analog Devices Inc.
 *
 * Licensed under the GPL-2 or later.
 */

#include <common.h>
#include <command.h>
#include <asm/arch/gpio_defs.h>
#include <asm/io.h>
#include <cl_fpga.h>
#include <fpga/fpga_gpio.h>

#ifdef CONFIG_FPGA

int do_fpgagpio(cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	if (argc != 3) {
 show_usage:
		printf("Usage:\n%s\n", cmdtp->usage);
		return 1;
	}

	struct fpga_device* fpgadev = get_fpgadev(CORE_ID_GPIO);
	if(!fpgadev) {
		printf("Unable to get FPGA device for gpio\n");
		return 1;
	}

	/* grab the <#> portion */
	unsigned short bank = 0;
	unsigned short mask = 0;
	int num_gpios = 0;
	tuConfigReg         config;

	unsigned short pin = simple_strtoul(argv[2], NULL, 10);
	unsigned short     *lpBaseReg = (unsigned short*)fpgadev->baseaddr;
	config.mnWord = lpBaseReg[gnCFG_OFFSET];
	num_gpios = config.msBits.mnNumBanks * config.msBits.mnNumIOPerBank;
	if (pin >= num_gpios)
		goto show_usage;
	bank = pin / config.msBits.mnNumIOPerBank;
	mask = 1<<(pin-(bank*config.msBits.mnNumIOPerBank));

	unsigned short val = lpBaseReg[gnBANK_OFFSET[bank]+gnIOVAL_OFFSET];

	/* parse the behavior */
	int port_cmd = argv[1][0];
	switch (port_cmd) {
		case 's':
		lpBaseReg[gnBANK_OFFSET[bank]+gnIOVAL_OFFSET] = val | mask;
		break;
		case 'c':
		lpBaseReg[gnBANK_OFFSET[bank]+gnIOVAL_OFFSET] = val & ~mask;
		break;
		case 'g':
		val = val & mask;
		break;
		default:  goto show_usage;
	}

	val = (lpBaseReg[gnBANK_OFFSET[bank]+gnIOVAL_OFFSET] & mask)?1:0;
	printf("gpio: pin %d = %d\n", pin,val);

	return 0;
}

U_BOOT_CMD(fpgagpio, 3, 0, do_fpgagpio,
	"fpgagpio    - set/clear/get gpio output pins\n",
	"<s|c|g> <gpio>\n"
	"    - set/clear/get the specified gpio\n");

#endif
