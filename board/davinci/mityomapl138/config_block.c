/*
 * config_block.c
 *
 *  Created on: Mar 18, 2010
 *      Author: mikew
 */
#include <common.h>
#include <command.h>
#include <image.h>
#include "config_block.h"
#include <spi_flash.h>
#include <asm/arch/hardware.h>
#include <asm/io.h>
#include <asm/errno.h>
#include <asm/arch/emac_defs.h>
#include <i2c.h>

#define CFG_BLOCK_SPI_BUS	0
#define CFG_BLOCK_SPI_CS	0
#define CFG_BLOCK_SPI_MAX_HZ	CONFIG_SF_DEFAULT_SPEED
#define CFG_BLOCK_SPI_MODE	SPI_MODE_3

extern phy_t phy;

static struct MityDSPL138Config default_config = {
		.ConfigMagicWord = CONFIG_MAGIC_WORD,
		.ConfigVersion = CONFIG_VERSION,
		.ConfigSizeBytes = sizeof(struct MityDSPL138Config),
		.Peripherals.Version = PERIPHERALS_VERSION,
		.Peripherals.Manufacturer = { 'C', 'r', 'i', 't', 'i', 'c', 'a', 'l', ' ', 'L', 'i', 'n', 'k', 0 },
		.Peripherals.ENETConfig.EnetConfig = ENET_CONFIG_MII,
		.Peripherals.ENETConfig.MACAddr = { 0x00, 0x50, 0xC2, 0x49, 0xDF, 0xFF },
		.Peripherals.ENETConfig.PHYMask = 3,
		.Peripherals.UARTConfig[0] = {
			.Enable = 0,
			.IsConsole = 0,
			.Baud = 115200
		},
		.Peripherals.UARTConfig[1] = {
			.Enable = 1,
			.IsConsole = 1,
			.Baud = 115200
		},
		.Peripherals.UARTConfig[2] = {
			.Enable = 0,
			.IsConsole = 0,
			.Baud = 115200
		},
		.Peripherals.SPIConfig[0] = {
			.Enable = 0,
			.CLKOut = 0,
			.CSEnable = { 0, 0, 0, 0, 0, 0, 0, 0},
			.ENAEnable = 0,
			.CLKRate = 0
		},
		.Peripherals.SPIConfig[1] = {
			.Enable = 1,
			.CLKOut = 1,
			.CSEnable = { 1, 0, 0, 0, 0, 0, 0, 0},
			.ENAEnable = 0,
			.CLKRate = 30000000
		},
		.Peripherals.LCDConfig = {
			.Enable = 0,
			.PanelName = { 'S','h','a','r','p','_','L','Q','0','3','5','Q','7','D','H','0','6', 0 },
		},
};

static struct I2CFactoryConfig default_factory_config = {
		.ConfigMagicWord = CONFIG_I2C_MAGIC_WORD,
		.ConfigVersion = CONFIG_I2C_VERSION,
		.MACADDR = { 0x00, 0x50, 0xC2, 0xBF, 0xDF, 0xFF },
		.SerialNumber = 100001,
		.FpgaType = 2,
		.PartNumber = { '8','0','-','0','0','0','2','5','8','R','C','-','1',' ','R','e','v',' ','B', 0 }
};

struct MityDSPL138Config config_block;
struct I2CFactoryConfig  factory_config_block;

typedef struct FPGAmapping {
	int   ID;
	char* Description;
} tsFPGAmapping;

tsFPGAmapping FPGAMAPPING[] = {
		{ FPGATYPE_NONE, "none" },
		{ FPGATYPE_XC6SLX9, "XC6SLX9" },
		{ FPGATYPE_XC6SLX16, "XC6SLX16" },
		{ FPGATYPE_XC6SLX25, "XC6SLX25" },
		{ FPGATYPE_XC6SLX45, "XC6SLX45" },
		{ FPGATYPE_UNKNOWN, "unknown" },
};

char* FPGA_String(int ID)
{
	int i;
	for (i = 0; FPGATYPE_UNKNOWN != FPGAMAPPING[i].ID; i++)
	{
		if (ID == FPGAMAPPING[i].ID)
			break;
	}
	return FPGAMAPPING[i].Description;
}

int put_config_block(void)
{
	int ret, i, sum;
	struct spi_flash *flash;
	unsigned char* tmp;

	ret = -1;

	/* get the FLASH controller ready */
	flash = spi_flash_probe(CFG_BLOCK_SPI_BUS, CFG_BLOCK_SPI_CS,
			CFG_BLOCK_SPI_MAX_HZ, CFG_BLOCK_SPI_MODE);
	if (!flash) {
		printf(" Error - unable to probe SPI flash.\n");
		goto err_probe;
	}

	/* verify the configuration block is sane */
	tmp = (unsigned char*)&config_block;
	sum = 0;
	for (i = 0; i < sizeof(config_block); i++)
	{
		sum += *tmp++;
	}

	ret = spi_flash_erase(flash, CONFIG_MITYDSP_ENV_OFFSET, CONFIG_MITYDSP_ENV_SIZE);
	if (ret) {
		printf("Error - unable to erase SPI FLASH sector : %d\n", ret);
		goto err_write;
	}

	/* write out the config block */
	ret = spi_flash_write(flash, CONFIG_MITYDSP_ENV_OFFSET, sizeof(config_block), &config_block);
	if (ret) {
		printf("Error - unable to write config block from SPI flash.\n");
		goto err_write;
	}

	ret = spi_flash_write(flash, CONFIG_MITYDSP_ENV_OFFSET+CONFIG_MITYDSP_ENV_SIZE-sizeof(int), sizeof(int), &sum);
	if (ret) {
		printf("Error - unable to read config block CRC from SPI flash.\n");
		goto err_write;
	}

err_write:
	/* cannot call free currently since the free function calls free() for
	 * spi_flash structure though it is not directly allocated through
	 * malloc()
	 */
	/* spi_flash_free(flash); */
err_probe:
	return ret;

}

int put_factory_config_block(void)
{
	int i, ret;
	u16 sum = 0;
	unsigned char* tmp;
	unsigned int addr = 0x00;

	tmp = (unsigned char*)&factory_config_block;

	for (i = 0; i < sizeof(factory_config_block); i++)
	{
		sum += *tmp++;
	}

	tmp = (unsigned char*)&factory_config_block;
	ret = 0;
	for (i = 0; i < sizeof(factory_config_block); i++)
	{
		ret |= i2c_write(0x50, addr++, 1, tmp++, 1);
		udelay(11000);
	}

	tmp = (unsigned char*)&sum;
	for (i = 0; i < 2; i++)
	{
		ret |= i2c_write(0x50, addr++, 1, tmp++, 1);
		udelay(11000);
	}

	if (ret) {
		puts("Error Writing I2C Configuration Block\n");
	}
	else {
		puts("I2C Factory Configuration Block Saved\n");
	}
	return ret;
}

int get_factory_config_block(void)
{
	unsigned char* tmp;
	int i;
	u16 sum, CheckSum;

	if (i2c_read(0x50, 0x00, 1, (unsigned char*)&factory_config_block, sizeof(factory_config_block)) != 0)
	{
		puts("error reading I2C Configuration Block\n");
		return -1;
	}

	if (i2c_read(0x50, sizeof(factory_config_block), 1, (unsigned char*)&CheckSum, 2) != 0)
	{
		puts("error reading I2C Configuration Block\n");
		return -1;
	}

	/* verify the configuration block is sane */
	tmp = (unsigned char*)&factory_config_block;
	sum = 0;
	for (i = 0; i < sizeof(factory_config_block); i++)
	{
		sum += *tmp++;
	}

	if (sum != CheckSum)
	{
		puts("Error - Factory Configuration Invalid\n");
		puts("You must set the factory configuration to make permanent\n");
		memcpy(&factory_config_block, &default_factory_config, sizeof(factory_config_block));
		return -1;
	}
	return 0;
}

int get_config_block(void)
{
	int ret, chk, i, sum;
	struct spi_flash *flash;
	unsigned char* tmp;

	ret = -1;

	/* get the FLASH controller ready */
	flash = spi_flash_probe(CFG_BLOCK_SPI_BUS, CFG_BLOCK_SPI_CS,
			CFG_BLOCK_SPI_MAX_HZ, CFG_BLOCK_SPI_MODE);
	if (!flash) {
		printf(" Error - unable to probe SPI flash.\n");
		goto err_probe;
	}

	/* read out the config block */
	ret = spi_flash_read(flash, CONFIG_MITYDSP_ENV_OFFSET, sizeof(config_block), &config_block);
	if (ret) {
		printf("Error - unable to read config block from SPI flash.\n");
		goto err_read;
	}

	ret = spi_flash_read(flash, CONFIG_MITYDSP_ENV_OFFSET+CONFIG_MITYDSP_ENV_SIZE-sizeof(int), sizeof(int), &chk);
	if (ret) {
		printf("Error - unable to read config block CRC from SPI flash.\n");
		goto err_read;
	}

	/* verify the configuration block is sane */
	tmp = (unsigned char*)&config_block;
	sum = 0;
	for (i = 0; i < sizeof(config_block); i++)
	{
		sum += *tmp++;
	}

	if (sum != chk)
	{
		puts("Error - Checksum on MityDSP-L138 Config Block Checksum Invalid\n");
		puts("You must set the config to FLASH to make permanent\n");
		memcpy(&config_block, &default_config, sizeof(config_block));
	}

err_read:
	/* cannot call free currently since the free function calls free() for
	 * spi_flash structure though it is not directly allocated through
	 * malloc()
	 */
	/* spi_flash_free(flash); */
err_probe:
	return ret;

}


/* This is a trivial atoi implementation since we don't have one available */
int atoi(char *string)
{
        int length;
        int retval = 0;
        int i;
        int sign = 1;

        length = strlen(string);
        for (i = 0; i < length; i++) {
                if (0 == i && string[0] == '-') {
                        sign = -1;
                        continue;
                }
                if (string[i] > '9' || string[i] < '0') {
                        break;
                }
                retval *= 10;
                retval += string[i] - '0';
        }
        retval *= sign;
        return retval;
}

char AsciiHexDigitVal (char anHexdigit)
{
    char rv = 0;

    if ((anHexdigit >= '0') && (anHexdigit <= '9'))
    {
        rv = (char)(anHexdigit - '0');
    }
    else if ((anHexdigit >= 'a') && (anHexdigit <= 'f'))
    {
        rv = (char)(anHexdigit - 'a' + 10);
    }
    else if((anHexdigit >= 'A') && (anHexdigit <= 'F'))
    {
        rv = (char)(anHexdigit - 'A' + 10);
    }

    return(rv);
}


int HexAtoi (char *apStr, unsigned int *apValue)
{
    unsigned int  val = 0;
    unsigned char digit;

    // catch NULL pointer
    if ((apStr == NULL) || (apValue == NULL)) return(-1);

    // read past hex indicator if present "0x"
    if ('0' == *apStr)
    {
        apStr++;
        if (('x' == *apStr) || ('X' == *apStr)) apStr++;
    }

    // process up to end of string
    while (*apStr != '\0')
    {
        digit = *apStr++;

        // process if valid digit
        if (((digit >= '0') && (digit <= '9')) ||
            ((digit >= 'a') && (digit <= 'f')) ||
            ((digit >= 'A') && (digit <= 'F')))
            val = (val * 16) + AsciiHexDigitVal(digit);  // hex
        else
            return(-1);
    }

    *apValue = val;
    return(0);
}

void get_u8 (char *buff, const char* desc, u8* val)
{
	int temp = *val;
	sprintf(buff,"%d", temp);
	readline_into_buffer(desc, buff);
	*val = (u8)atoi(buff);
}

int do_config (cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	char buffer[80];

	if (argc == 1) {
		/* List configuration info */
			puts ("MityDSP-L138 Configuration:\n");
			printf("Config Version  : %d.%d\n", config_block.ConfigVersion>>16, config_block.ConfigVersion&0xFFFF);
			printf("Config Size     : %d\n", config_block.ConfigSizeBytes);
			printf("Manufacturer    : %s\n", config_block.Peripherals.Manufacturer);
			printf("Ethernet Config : %d\n", config_block.Peripherals.ENETConfig.EnetConfig);
			printf("Ethernet Name   : %s\n", phy.name);
			printf("Ethernet PHYMask: %x\n", phy.mask);
			printf("LCD Config      : %d\n", config_block.Peripherals.LCDConfig.Enable);
			if (config_block.Peripherals.LCDConfig.Enable)
				printf("   LCD Panel     : %s\n", config_block.Peripherals.LCDConfig.PanelName);
			printf("MMC0 Config     : %d\n", config_block.Peripherals.MMCConfig[0].Enable);
			printf("MMC1 Config     : %d\n", config_block.Peripherals.MMCConfig[1].Enable);
	} else {
		unsigned int i;
		unsigned int j;

		if (0 == strncmp(argv[1],"set",3)) {
			sprintf(buffer, "%d", config_block.Peripherals.ENETConfig.EnetConfig);
			readline_into_buffer ("Ethernet Config : ", buffer);
			i = atoi(buffer);
			if ((i > 0) && (i >= ENET_CONFIG_NONE) && (i <= ENET_CONFIG_RMII))
				config_block.Peripherals.ENETConfig.EnetConfig = i;

			for (j = 0; j < 3; j++) {
				printf("UART - %d Configuration ---\n", j);
				sprintf(buffer, "%d", config_block.Peripherals.UARTConfig[j].Enable);
				readline_into_buffer("Enable UART : ", buffer);
				i = atoi(buffer);
				config_block.Peripherals.UARTConfig[j].Enable = i ? 1 : 0;
				if (i) {
					sprintf(buffer,"%d", config_block.Peripherals.UARTConfig[j].Baud);
					readline_into_buffer("Baud : ",buffer);
					config_block.Peripherals.UARTConfig[j].Baud = atoi(buffer);

					sprintf(buffer,"%d", config_block.Peripherals.UARTConfig[j].EnableHWFlowCtrl);
					readline_into_buffer("HW Flow Control : ", buffer);
					config_block.Peripherals.UARTConfig[j].EnableHWFlowCtrl = atoi(buffer);

					sprintf(buffer,"%d", config_block.Peripherals.UARTConfig[j].IsConsole);
					readline_into_buffer("Is Console : ", buffer);
					config_block.Peripherals.UARTConfig[j].IsConsole = atoi(buffer);
				}
			}
			for (j = 0; j < 2; j++) {
				printf("SPI - %d Configuration ---\n", j);
				sprintf(buffer, "%d", config_block.Peripherals.SPIConfig[j].Enable);
				readline_into_buffer("Enable SPI : ", buffer);
				i = atoi(buffer);
				config_block.Peripherals.SPIConfig[j].Enable = i ? 1 : 0;
				if (i) {
					int k;
					sprintf(buffer,"%d", config_block.Peripherals.SPIConfig[j].CLKOut);
					readline_into_buffer("CLK is Output : ",buffer);
					config_block.Peripherals.SPIConfig[j].CLKOut = atoi(buffer);

					for (k = 0; k < 8; k++)
					{
						char buf2[32];
						sprintf(buffer,"%02X", config_block.Peripherals.SPIConfig[j].CSEnable[k]);
						sprintf(buf2, "Chip Select [%d] Enable : ", k);
						readline_into_buffer(buf2, buffer);
						config_block.Peripherals.SPIConfig[j].CSEnable[k] = atoi(buffer);
					}

					sprintf(buffer,"%d", config_block.Peripherals.SPIConfig[j].ENAEnable);
					readline_into_buffer("ENA ENable : ", buffer);
					config_block.Peripherals.SPIConfig[j].ENAEnable = atoi(buffer);

					sprintf(buffer,"%d", config_block.Peripherals.SPIConfig[j].CLKRate);
					readline_into_buffer("CLK Rate : ", buffer);
					config_block.Peripherals.SPIConfig[j].CLKRate = atoi(buffer);
				}
			}
			puts("LCD Configuration\n");
			sprintf(buffer,"%d", config_block.Peripherals.LCDConfig.Enable);
			readline_into_buffer("LCD Enable : ", buffer);
			config_block.Peripherals.LCDConfig.Enable = atoi(buffer);

			if (config_block.Peripherals.LCDConfig.Enable)
			{
				sprintf(buffer,"%s",config_block.Peripherals.LCDConfig.PanelName);
				readline_into_buffer("Panel Name : ", buffer);
				strncpy((char*)&config_block.Peripherals.LCDConfig.PanelName[0], buffer, 31);
				config_block.Peripherals.LCDConfig.PanelName[31] = 0;
			}
			puts("MMC 0 Configuration\n");
			get_u8 (buffer, "MMC 0 Enable : ", &config_block.Peripherals.MMCConfig[0].Enable);
			if (config_block.Peripherals.MMCConfig[0].Enable)
			{
				get_u8 (buffer, "Write Protect Pin bank : ", 
					&config_block.Peripherals.MMCConfig[0].WriteProtect.Bank);
				get_u8 (buffer, "Write Protect Pin bank offset : ", 
					&config_block.Peripherals.MMCConfig[0].WriteProtect.Pin);
				get_u8 (buffer, "Card Detect Pin bank : ", 
					&config_block.Peripherals.MMCConfig[0].CardDetect.Bank);
				get_u8 (buffer, "Card Detect Pin bank offset : ", 
					&config_block.Peripherals.MMCConfig[0].CardDetect.Pin);
			}

		} else if (0 == strncmp(argv[1],"save",4)) {
			put_config_block();
			puts("Configuration Saved\n");
		}
		else {
			puts("Unknown Option\n");
		}
	}

	return 0;
}

void get_board_serial(struct tag_serialnr *sn)
{
	sn->low = factory_config_block.SerialNumber;
	sn->high = 0;
}

void setup_peripherals_tag(struct tag **tmp)
{
	struct tag *params = *tmp;
	params->hdr.tag = ATAG_PERIPHERALS;
	params->hdr.size = tag_size (tag_peripherals);
	memcpy(&config_block.Peripherals.ENETConfig.MACAddr[0], &factory_config_block.MACADDR[0],6);
	config_block.Peripherals.ENETConfig.PHYMask = phy.mask;
	memcpy(&params->u.cmdline.cmdline[0], &config_block.Peripherals, sizeof(struct tag_peripherals));
	params = tag_next (params);
	*tmp = params;
}

/***************************************************/

U_BOOT_CMD(
	config,	CONFIG_SYS_MAXARGS,	0,	do_config,
	"mitydsp-l138 config block operations",
	     "    - print current configuration\n"
	"config set\n"
	"         - set new configuration\n"
	"config save\n"
	"         - write new configuration to SPI FLASH\n"
);

int do_factoryconfig (cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	char buffer[80];

	if (argc == 1) {
		/* List configuration info */
			puts ("Factory Configuration:\n");
			printf("Config Version : %d.%d\n", factory_config_block.ConfigVersion>>16, factory_config_block.ConfigVersion&0xFFFF);
			printf("MAC Address    : %02X:%02X:%02X:%02X:%02X:%02X\n",
					factory_config_block.MACADDR[0],
					factory_config_block.MACADDR[1],
					factory_config_block.MACADDR[2],
					factory_config_block.MACADDR[3],
					factory_config_block.MACADDR[4],
					factory_config_block.MACADDR[5]);
			printf("Serial Number  : %d\n", factory_config_block.SerialNumber);
			printf("FPGA Type      : %d [%s]\n", factory_config_block.FpgaType, FPGA_String(factory_config_block.FpgaType));
			printf("Part Number    : %s\n", factory_config_block.PartNumber);
	} else {
		unsigned int i;
		char* p;
		if (0 == strncmp(argv[1],"set",3)) {
			sprintf(buffer, "%02X:%02X:%02X:%02X:%02X:%02X",
					factory_config_block.MACADDR[0],
					factory_config_block.MACADDR[1],
					factory_config_block.MACADDR[2],
					factory_config_block.MACADDR[3],
					factory_config_block.MACADDR[4],
					factory_config_block.MACADDR[5]);
			readline_into_buffer ("MAC Address  : ", buffer);
			p = buffer;
			for (i = 0; i < 6; i++) {
				int j = 0;
				while(p[j] && (p[j] != ':')) j++;
				if (1) {
					int r;
					unsigned int t;
					char temp = p[j];
					p[j] = 0;
					r = HexAtoi (p, &t);
					p[j] = temp;
					factory_config_block.MACADDR[i] = t&0xFF;
				}
				p = &p[j];
				if (*p) p++;
			}
			sprintf(buffer, "%d", factory_config_block.SerialNumber);
			readline_into_buffer ("Serial Number :", buffer);
			i = atoi(buffer);
			if (i > 0) factory_config_block.SerialNumber = i;
			sprintf(buffer, "%s", factory_config_block.PartNumber);
			readline_into_buffer ("Part Number   :", buffer);
			memcpy(factory_config_block.PartNumber, buffer, sizeof(factory_config_block.PartNumber));
			factory_config_block.PartNumber[sizeof(factory_config_block.PartNumber)-1] = 0;
			sprintf(buffer, "%d", factory_config_block.FpgaType);
			readline_into_buffer ("FPGA Type     :", buffer);
			factory_config_block.FpgaType = atoi(buffer);
		} else if (0 == strncmp(argv[1],"save",4)) {
			put_factory_config_block();
			puts("Configuration Saved\n");
		}
		else {
			puts("Unknown Option\n");
		}
	}

	return 0;
}

U_BOOT_CMD(
	factoryconfig,	CONFIG_SYS_MAXARGS,	0,	do_factoryconfig,
	"mitydsp-l138 factory config block operations",
	     "    - print current configuration\n"
	"factoryconfig set\n"
	"         - set new configuration\n"
	"factoryconfig save\n"
	"         - write new configuration to I2C FLASH\n"
);

