/*
 * fpga.c
 *
 *  Created on: Apr 6, 2010
 *      Author: mitydspl138
 */
#include <common.h>
#include <command.h>
#include <malloc.h>
#include <asm/arch/gpio_defs.h>
#include <asm/io.h>
/* defining FPGA_CTRL_C causes strings, etc to be defined in core_ids.h */
#define FPGA_CTRL_C
#include <cl_fpga.h>

extern int HexAtoi (char *apStr, unsigned int *apValue);

/* MityDSP-xxxF FPGA utils */
static char* fpga_baseaddr = NULL;
static const unsigned int MAX_CORE_NUM = 31; //!< Maximum possible allowed core
	//!< number.

/**
 *  Retrieve human readable core description.
 *
 *  \param[in] ID core number
 *  \return string description of core
 */
const char* fpga_core_name(unsigned char ID)
{
	int i = 0;
	for (i = 0; i < ARRAY_SIZE(KnownCores); i++)
	{
		if (ID == KnownCores[i].ID)
		{
			return KnownCores[i].Name;
		}
	}
	return "Unknown";
}

/**
 *  Reads the core version information out of a spot in the
 *  FPGA.
 *
 *  \param[in] baseaddr location of the core version register
 *  \param[in] pdata location to store the core version data
 *
 *  \return non-zero if the core data is invalid
 */
int read_core_version(void* baseaddr, struct coreversion* pdata)
{
	int      i;
	corever0 ver;
	int      found = 0;
	int      rv = -1;

	for (i = 0; i < 4; i++)
	{
		ver.word = *(uint16_t*)(baseaddr);
		switch(ver.bits.FIFO_no)
		{
		case 0:
			pdata->ver0.word = ver.word;
			break;
		case 1:
			pdata->ver1.word = ver.word;
			break;
		case 2:
			pdata->ver2.word = ver.word;
			break;
		case 3:
			pdata->ver3.word = ver.word;
			break;
		}
		found |= (1<<ver.bits.FIFO_no);
	}
	if (found == 0x0F)
		rv = 0;
	return rv;
}

/* Probe the FPGA for cores and try to find the matching core to the provided
 * ID. If -1 is used for core_id, just print the cores found.
 * \param[in] core_id - FPGA core ID to match
 * \return pointer to fpga_device struct (caller owned) or NULL if not found.
 */
struct fpga_device* get_fpgadev(int core_id)
{
	int ii;
	struct fpga_device* rv = NULL;
	if(!fpga_baseaddr) {
		printf("ERROR! %s called without FPGA loaded\n",__FUNCTION__);
		return NULL;
	}
	for (ii = 0; ii < CONFIG_FPGA_NUM_CORES; ii++)
	{
		struct coreversion cv;
		void* baseaddr = (void*)(fpga_baseaddr+CONFIG_FPGA_CORE_OFFSET*ii);
		if (0 == read_core_version(baseaddr,&cv))
		{
			if(-1 == core_id) {
			printf( "Found Device ID %02d-%s (%02d.%02d) at %08X [%d]\n",
				cv.ver0.bits.core_id,
				fpga_core_name(cv.ver0.bits.core_id),
				cv.ver1.bits.major, cv.ver1.bits.minor,
				(unsigned int)baseaddr, ii);
			}
			else if(core_id == cv.ver0.bits.core_id)
			{
				rv = (struct fpga_device*)malloc(sizeof(struct fpga_device));

				memset(rv, 0, sizeof(struct fpga_device));

				rv->baseaddr = baseaddr;
				rv->coreversion = cv;
				rv->coreindex = ii;
				rv->core_id = core_id;
				break;
			}
		}
	}
	return rv;

}
int do_loadfpga (cmd_tbl_t * cmdtp, int flag, int argc, char *argv[])
{
	int rv = 0;
	unsigned int i;
	int pdisable = 0;
	unsigned int  length = 0x00080000; /* TODO - default length to size of FPGA */
	unsigned char* addr;
	unsigned char* w_addr;
	volatile unsigned char* fpga_ptr = (volatile unsigned char*)0x66000000; /* CE 5 space - TODO fix hardcode */

	if(getenv("fpga_baseaddr")) {
		unsigned int baddr = 0;
		HexAtoi (getenv("fpga_baseaddr"), &baddr);
		if(baddr)
			fpga_ptr = (volatile unsigned char*)baddr;
	}
	if (argc > 3) {
		puts("Disabling PROGRAM PIN\n");
		pdisable = 1;
	}
	if (argc > 2) {
		HexAtoi (argv[2], &length);
	}

	if (argc >= 2) {
		unsigned int foo;
		unsigned int tmp;
		HexAtoi(argv[1], &tmp);
		addr = (unsigned char*)tmp;
		printf("Loading FPGA from 0x%08X with 0x%X bytes\n", (unsigned int)addr, length);
		/* set RDWR pin for write mode */
		davinci_gpio_bank23->out_data &= ~(1 << (9+16)); /* RDWR = 0 */
		/* assert program pin */
		if (!pdisable)
		{
			davinci_gpio_bank67->dir &= ~(1 << 15); /* FPGA_PROGRAM = output (for now) */
			davinci_gpio_bank67->out_data &= ~(1 << 15); /* FPGA_PROGRAM = 0 */
			udelay(1000); /* wait for init to go low */
// Mitydsp:
//			foo = davinci_gpio_bank01->in_data&(1 << (15+16)); /* read FPGA_INIT pin */
//			foo >>=31
// KLANG:technologies Protoype:
			foo = davinci_gpio_bank01->in_data&(1 << (8)); /* read FPGA_INIT pin */
			foo >>=8;
			foo &= 1;
			if (foo)
			{
				puts("WARNING: INIT Pin Did Not go low after 1 ms\n");
			}
			davinci_gpio_bank67->out_data |= (1 << 15); /* FPGA_PROGRAM = 1 */
		}
		/* wait for initialization to complete before loading */
		udelay(1000);
// Mitydsp:
//			foo = davinci_gpio_bank01->in_data&(1 << (15+16)); /* read FPGA_INIT pin */
//			foo >>=31
// KLANG:technologies Protoype:
		foo = davinci_gpio_bank01->in_data&(1 << (8)); /* read FPGA_INIT pin */
		foo >>=8;
		foo &= 1;
		if (!foo)
		{
			puts("WARNING: INIT Pin is Low at start of data cycle\n");
		}
		w_addr = addr;
		/* write the data to the fpga */
		for (i = 0; i < length; i++)
		{
			*fpga_ptr = *w_addr++;
		}
		/* set read write back to "read" */
		davinci_gpio_bank23->out_data |= (1 << (9+16)); /* RDWR = 1 */
		/* set FPGA program back to input state (it's pulled high on the board) */
		davinci_gpio_bank67->dir |= (1 << 15); /* FPGA_PROGRAM = input (for now) */
		puts("Loading FPGA done\n");
		fpga_baseaddr = (char*)(CONFIG_FPGA_BASE_ADDR);
		(void)get_fpgadev(-1); /* print cores */
	} else {
		puts("Invalid Address\n");
		rv = -1;
	}
	return rv;
}
/***************************************************/

U_BOOT_CMD(
	loadfpga,	CONFIG_SYS_MAXARGS,	0,	do_loadfpga,
	"loadfpga addr [length] [np]",
	     "    - load fpga with data at addr (hex) of length (hex) bytes\n"
	     "    - if np is set, the PROGRAM_PIN will not be pulled (for debugging)\n"
	     "\n\n---This function is modified by KLANG:technologies based on the Code from CriticalLink---\n\n"
);

