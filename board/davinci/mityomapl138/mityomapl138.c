/*
 * Copyright (c) 2010 Critical Link, LLC <michael.williamson@criticallink.com>
 *
 * Based on code from Sources Below.  Original notices follow.
 *
 * Copyright (C) 2009 Nick Thompson, GE Fanuc, Ltd. <nick.thompson@gefanuc.com>
 *
 * Base on code from TI. Original Notices follow:
 *
 * (C) Copyright 2008, Texas Instruments, Inc. http://www.ti.com/
 *
 * Modified for DA8xx EVM.
 *
 * Copyright (C) 2007 Sergey Kubushyn <ksi@koi8.net>
 *
 * Parts are shamelessly stolen from various TI sources, original copyright
 * follows:
 * -----------------------------------------------------------------
 *
 * Copyright (C) 2004 Texas Instruments.
 *
 * ----------------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * ----------------------------------------------------------------------------
 *
 * Modified by KLANG:technologies GmbH - 2014/06/10 'Bootzeit optimierungen, UBIFS, Compiler Parameter'
 *
 */

#include <common.h>
#include <i2c.h>
#include <spi.h>
#include <net.h>
#include <spi_flash.h>
#include <asm/arch/hardware.h>
#include <asm/io.h>
#include <asm/errno.h>
#include <nand.h>
#include <asm/arch/nand_defs.h>
#include <asm/arch/emif_defs.h>
#include <asm/arch/gpio_defs.h>
#include "../common/misc.h"
#include "config_block.h"
#include <mmc.h>
#include <asm/arch/sdmmc_defs.h>
DECLARE_GLOBAL_DATA_PTR;

#define pinmux  &davinci_syscfg_regs->pinmux

static emif_registers *const emif_regs = (void *) DAVINCI_ASYNC_EMIF_CNTRL_BASE;
#ifdef CONFIG_DRIVER_TI_EMAC_USE_RMII
extern unsigned int using_rmii;
#endif

/* SPI0 pin muxer settings */
const struct pinmux_config mmc0_pins[] = {
        { pinmux[10], 2, 0 },   /* MMCSD0_CLK */
        { pinmux[10], 2, 1 },   /* MMCSD0_CMD */
        { pinmux[10], 2, 2 },   /* MMCSD0_DAT_0 */
        { pinmux[10], 2, 3 },   /* MMCSD0_DAT_1 */
        { pinmux[10], 2, 4 },   /* MMCSD0_DAT_2 */
        { pinmux[10], 2, 5 },   /* MMCSD0_DAT_3 */
        /* DA850 supports only 4-bit mode, remaining pins are not configured */
};

#define pinmux	&davinci_syscfg_regs->pinmux

/* SPI0 pin muxer settings */
const struct pinmux_config spi1_pins[] = {
	{ pinmux[5], 1, 0 }, /*cs1 */
	{ pinmux[5], 1, 1 }, /*cs0 */
	{ pinmux[5], 1, 2 }, /* */
	{ pinmux[5], 1, 4 }, /* */
	{ pinmux[5], 1, 5 }, /* */
};

/* UART pin muxer settings (changed to UART 1 from UART 2) */
const struct pinmux_config uart_pins[] = {
	{ pinmux[0], 4, 4 }, /* UART1 CTS */
	{ pinmux[0], 4, 5 }, /* UART1 RTS */
	{ pinmux[4], 2, 6 }, /* UART1 RxD */
	{ pinmux[4], 2, 7 }  /* UART1 TxD */
};


const struct pinmux_config emac_rmii_pins[] = {
	{ pinmux[14], 8, 2 },	/* RMII_TXD1 */
	{ pinmux[14], 8, 3 },	/* RMII_TXD0 */
	{ pinmux[14], 8, 4 },	/* RMII_TXEN */
	{ pinmux[14], 8, 5 },	/* RMII_RXD1 */
	{ pinmux[14], 8, 6 },	/* RMII_RXD0 */
	{ pinmux[14], 8, 7 },	/* RMII_RXER */
	{ pinmux[15], 0, 0 },	/* RMII_MHZ_50 8 is source from PLL0_SYSCLK7, 0 is input from external oscillator*/
	{ pinmux[15], 8, 1 },	/* RMII_CRS_DV */
	{ pinmux[4], 8, 0 },	/* MDIO_CLK */
	{ pinmux[4], 8, 1 }	/* MDIO_D */
};

/**
 *   Reset pin for the PHY. This is called out separately as we need to
 *   toggle the reset line before assigning the rest of the pins so they are
 *   tri-stated
 */
const struct pinmux_config emac_mii_reset_pins[] = {
	/* these pins, when reset, come up tri-stated.  should not
	 * need to reconfigure them.
	 */
#if 0
	{ pinmux[2], 4, 3 }, /* GPIO1[12] (MII_COL) */
	{ pinmux[3], 4, 4 }, /* GPIO8[4] (MII_RXD3) */
	{ pinmux[3], 4, 5 }, /* GPIO8[3] (MII_RXD2) */
	{ pinmux[3], 4, 6 }, /* GPIO8[2] (MII_RXD1) */
	{ pinmux[3], 4, 7 }, /* GPIO8[1] (MII_RXD0) */
#endif
	{ pinmux[11],8, 0 }  /* GPIO5[15] */
};

const struct pinmux_config emac_mii_pins[] = {
	{ pinmux[2], 8, 1 }, /* MII_TXEN */
	{ pinmux[2], 8, 2 }, /* MII_TXCLK */
	{ pinmux[2], 8, 3 }, /* MII_COL */
	{ pinmux[2], 8, 4 }, /* MII_TXD3 */
	{ pinmux[2], 8, 5 }, /* MII_TXD2 */
	{ pinmux[2], 8, 6 }, /* MII_TXD1 */
	{ pinmux[2], 8, 7 }, /* MII_TXD0 */
	{ pinmux[3], 8, 0 }, /* MII_RXCLK */
	{ pinmux[3], 8, 1 }, /* MII_RXDV */
	{ pinmux[3], 8, 2 }, /* MII_RXER */
	{ pinmux[3], 8, 3 }, /* MII_CRS */
	{ pinmux[3], 8, 4 }, /* MII_RXD3 */
	{ pinmux[3], 8, 5 }, /* MII_RXD2 */
	{ pinmux[3], 8, 6 }, /* MII_RXD1 */
	{ pinmux[3], 8, 7 }, /* MII_RXD0 */
	{ pinmux[4], 8, 0 }, /* MDIO_CLK */
	{ pinmux[4], 8, 1 }, /* MDIO_D */
};

const struct pinmux_config emac_rmii_reset_pins[] = {
	{ pinmux[13], 8, 5 }, /* GPIO6[10] PHY RESET, pulled up by default */
};

/* I2C pin muxer settings */
const struct pinmux_config i2c_pins[] = {
	{ pinmux[4], 2, 2 },
	{ pinmux[4], 2, 3 }
};

/* GPIO Pins needed for FPGA programming and interfacing */
const struct pinmux_config fpga_pins[] = {
	{ pinmux[13], 8, 0},  /* FPGA_PROGRAM -> GP6[15] */
	{ pinmux[0],  8, 7},  /* FPGA_INIT    -> GP0[08] */ //KLANG:
//	{ pinmux[2],  8, 0},  /* FPGA_INIT    -> GP1[15] */ Mitydsp
	{ pinmux[13], 8, 3},  /* FPGA_INT0    -> GP6[12] */
	{ pinmux[13], 8, 2},  /* FPGA_INT1    -> GP6[13] */
	{ pinmux[7],  8, 6},  /* (EMA_RNW) RDWR_B -> GP3[9] */
};

const struct pinmux_config aemif_pins[] = {
	{ pinmux[7], 1, 0 }, /* EMA_CS[2] */
	{ pinmux[7], 1, 1 }, /* EMA_CS[3] */
	{ pinmux[7], 1, 2 }, /* EMA_CS[4] */
	{ pinmux[7], 1, 3 }, /* EMA_CS[5] */
	{ pinmux[7], 1, 4 }, /* EMA_WE */
	{ pinmux[7], 1, 5 }, /* EMA_OE */
	{ pinmux[7], 1, 7 }, /* EMA_WAIT */
	{ pinmux[8], 1, 0 }, /* EMA_D[15] */
	{ pinmux[8], 1, 1 }, /* EMA_D[14] */
	{ pinmux[8], 1, 2 }, /* EMA_D[13] */
	{ pinmux[8], 1, 3 }, /* EMA_D[12] */
	{ pinmux[8], 1, 4 }, /* EMA_D[11] */
	{ pinmux[8], 1, 5 }, /* EMA_D[10] */
	{ pinmux[8], 1, 6 }, /* EMA_D[9] */
	{ pinmux[8], 1, 7 }, /* EMA_D[8] */
	{ pinmux[9], 1, 0 }, /* EMA_D[7] */
	{ pinmux[9], 1, 1 }, /* EMA_D[6] */
	{ pinmux[9], 1, 2 }, /* EMA_D[5] */
	{ pinmux[9], 1, 3 }, /* EMA_D[4] */
	{ pinmux[9], 1, 4 }, /* EMA_D[3] */
	{ pinmux[9], 1, 5 }, /* EMA_D[2] */
	{ pinmux[9], 1, 6 }, /* EMA_D[1] */
	{ pinmux[9], 1, 7 }, /* EMA_D[0] */
	{ pinmux[11], 1, 2 }, /* EMA_A[13] */
	{ pinmux[11], 1, 3 }, /* EMA_A[12] */
	{ pinmux[11], 1, 4 }, /* EMA_A[11] */
	{ pinmux[11], 1, 5 }, /* EMA_A[10] */
	{ pinmux[11], 1, 6 }, /* EMA_A[9] */
	{ pinmux[11], 1, 7 }, /* EMA_A[8] */
	{ pinmux[12], 1, 0 }, /* EMA_A[7] */
	{ pinmux[12], 1, 1 }, /* EMA_A[6] */
	{ pinmux[12], 1, 2 }, /* EMA_A[5] */
	{ pinmux[12], 1, 3 }, /* EMA_A[4] */
	{ pinmux[12], 1, 4 }, /* EMA_A[3] */
	{ pinmux[12], 1, 5 }, /* EMA_A[2] */
	{ pinmux[12], 1, 6 }, /* EMA_A[1] */
	{ pinmux[12], 1, 7 }, /* EMA_A[0] */
	{ pinmux[6],  1, 0 }, /* EMA_CLK */
	{ pinmux[5],  1, 7 }, /* EMA_BA[0] */
	{ pinmux[5],  1, 6 }, /* EMA_BA[1] */
	{ pinmux[6],  1, 5 }, /* EMA_WE_DQM[1] */
	{ pinmux[6],  1, 4 }, /* EMA_WE_DQM[0] */
};

int board_init(void)
{
#ifndef CONFIG_USE_IRQ
	/*
	 * Mask all IRQs by clearing the global enable and setting
	 * the enable clear for all the 90 interrupts.
	 */

	writel(0, &davinci_aintc_regs->ger);

	writel(0, &davinci_aintc_regs->hier);

	writel(0xffffffff, &davinci_aintc_regs->ecr1);
	writel(0xffffffff, &davinci_aintc_regs->ecr2);
	writel(0xffffffff, &davinci_aintc_regs->ecr3);
#endif

	/* arch number of the board */
	gd->bd->bi_arch_number = MACH_TYPE_MITYOMAPL138;

	/* address of boot parameters */
	gd->bd->bi_boot_params = LINUX_BOOT_PARAM_ADDR;

	/*
	 * Power on required peripherals
	 * ARM does not have access by default to PSC0 and PSC1
	 * assuming here that the DSP bootloader has set the IOPU
	 * such that PSC access is available to ARM
	 */
	lpsc_on(DAVINCI_LPSC_AEMIF);    /* NAND, NOR */
	lpsc_on(DAVINCI_LPSC_SPI1);     /* Serial Flash */
	lpsc_on(DAVINCI_LPSC_EMAC);     /* image download */
	lpsc_on(DAVINCI_LPSC_UART1);    /* console */
	lpsc_on(DAVINCI_LPSC_GPIO);
	lpsc_on(DAVINCI_LPSC_MMC_SD);

	/* setup the SUSPSRC for ARM to control emulation suspend */
	writel(readl(&davinci_syscfg_regs->suspsrc) &
	       ~(DAVINCI_SYSCFG_SUSPSRC_EMAC | DAVINCI_SYSCFG_SUSPSRC_I2C |
		 DAVINCI_SYSCFG_SUSPSRC_SPI1 | DAVINCI_SYSCFG_SUSPSRC_TIMER0 |
		 DAVINCI_SYSCFG_SUSPSRC_UART1),
	       &davinci_syscfg_regs->suspsrc);

	if (davinci_configure_pin_mux(spi1_pins, ARRAY_SIZE(spi1_pins)) != 0)
		return 1;

	if (davinci_configure_pin_mux(uart_pins, ARRAY_SIZE(uart_pins)) != 0)
		return 1;

	if (davinci_configure_pin_mux(i2c_pins, ARRAY_SIZE(i2c_pins)) != 0)
		return 1;

	/* default FPGA IO pins to sane state */
	davinci_gpio_bank67->out_data |= (1 << 15); /* FPGA_PROGRAM = 1 */
	davinci_gpio_bank67->dir |= (1 << 15); /* FPGA_PROGRAM = input (for now) */
	davinci_gpio_bank23->out_data |= (1 << (9+16)); /* RDWR = 1 */
	davinci_gpio_bank23->dir &= ~(1 << (9+16)); /* RDWR = output */
	davinci_gpio_bank67->dir |= (1 << 12); /* FPGA_INT0 = input */
	davinci_gpio_bank67->dir |= (1 << 13); /* FPGA_INT1 = input */
	davinci_gpio_bank01->dir |= (1 << (8)); /* FPGA_INIT = input */ // KLANG
	davinci_gpio_bank01->dir |= (1 << (15+16)); /* FPGA_INIT = input */ // Mitydsp

	if (davinci_configure_pin_mux(fpga_pins, ARRAY_SIZE(fpga_pins)) != 0)
		return 1;

	if (davinci_configure_pin_mux(aemif_pins, ARRAY_SIZE(aemif_pins)) != 0)
		return 1;

	/* setup the EMIFA configuration registers */
	emif_regs->AB1CR = /* CE 2 address space config register */
			(0 << 31)    | /* Select Strobe */
			(0 << 30)    | /* Extended Wait */
			(0x0F << 26) | /* Write Setup-1 in EMA_CLK */
			(0x3F << 20) | /* Write strobe-1 in EMA_CLK */
			(0x7  << 17) | /* Write Hold-1 in EMA_CLK */
			(0x0F << 13) | /* Read Setup-1 in EMA_CLK */
			(0x3F << 7)  | /* Read Strobe-1 in EMA_CLK */
			(0x7 << 4)   | /* Read Hold-1 in EMA_CLK */
			(3 << 2)     | /* Turn-Around in EMA_CLK */
			(1);           /* Bus Width (16 bits data bus) */

	emif_regs->AB2CR = /* CE 3 address space config register (NAND) */
			(0 << 31)    | /* Select Strobe */
			(0 << 30)    | /* Extended Wait */
			(0x02 << 26) | /* Write Setup-1 in EMA_CLK */
			(0x01 << 20) | /* Write strobe-1 in EMA_CLK */
			(0x2  << 17) | /* Write Hold-1 in EMA_CLK */
			(0x02 << 13) | /* Read Setup-1 in EMA_CLK */
			(0x02 << 7)  | /* Read Strobe-1 in EMA_CLK */
			(0x1 << 4)   | /* Read Hold-1 in EMA_CLK */
			(3 << 2)     | /* Turn-Around in EMA_CLK */
			(0);           /* Bus Width (8 bits data bus) */

	emif_regs->AB3CR = /* CE 4 address space config register */
			(0 << 31)    | /* Select Strobe */
			(0 << 30)    | /* Extended Wait */
			(0x0 << 26)  | /* Write Setup-1 in EMA_CLK */
			(0x0 << 20)  | /* Write strobe-1 in EMA_CLK */
			(0x0  << 17) | /* Write Hold-1 in EMA_CLK */
			(0x0 << 13)  | /* Read Setup-1 in EMA_CLK */
			(0x5 << 7)   | /* Read Strobe-1 in EMA_CLK */
			(0x0 << 4)   | /* Read Hold-1 in EMA_CLK */
			(0 << 2)     | /* Turn-Around in EMA_CLK */
			(1);           /* Bus Width (16 bits data bus) */

	emif_regs->AB4CR = /* CE 5 address space config register */
			(0 << 31)    | /* Select Strobe */
			(0 << 30)    | /* Extended Wait */
			(0x0 << 26)  | /* Write Setup-1 in EMA_CLK */
			(0x0 << 20)  | /* Write strobe-1 in EMA_CLK */
			(0x0  << 17) | /* Write Hold-1 in EMA_CLK */
			(0x0 << 13)  | /* Read Setup-1 in EMA_CLK */
			(0x5 << 7)   | /* Read Strobe-1 in EMA_CLK */
			(0x0 << 4)   | /* Read Hold-1 in EMA_CLK */
			(0 << 2)     | /* Turn-Around in EMA_CLK */
			(1);           /* Bus Width (16 bits data bus) */

	/* enable the console UART */
	writel((DAVINCI_UART_PWREMU_MGMT_FREE | DAVINCI_UART_PWREMU_MGMT_URRST |
		DAVINCI_UART_PWREMU_MGMT_UTRST),
	       &davinci_uart1_ctrl_regs->pwremu_mgmt);

	return(0);
}

void dsp_lpsc_on(unsigned domain, unsigned int id)
{
	dv_reg_p mdstat, mdctl, ptstat, ptcmd;
	struct davinci_psc_regs *psc_regs;

	psc_regs = davinci_psc0_regs;
	mdstat = &psc_regs->psc0.mdstat[id];
	mdctl = &psc_regs->psc0.mdctl[id];
	ptstat = &psc_regs->ptstat;
	ptcmd = &psc_regs->ptcmd;

	while (*ptstat & (0x1 << domain)) {;}

	if ((*mdstat & 0x1f) == 0x03)
		return;                 /* Already on and enabled */

	*mdctl |= 0x03;

	*ptcmd = 0x1 << domain;

	while (*ptstat & (0x1 << domain)) {;}
	while ((*mdstat & 0x1f) != 0x03) {;}    /* Probably an overkill... */
}

static void dspwake(void)
{
	unsigned *resetvect = (unsigned *)DAVINCI_L3CBARAM_BASE;

	/* if the device is ARM only, return */
	if ((REG(CHIP_REV_ID_REG) & 0x3f) == 0x10)
		return;

	if (!strcmp(getenv("dspwake"), "no"))
		return;

	*resetvect++ = 0x1E000; /* DSP Idle */
	/* clear out the next 10 words as NOP */
	memset(resetvect, 0, sizeof(unsigned) * 10);

	/* setup the DSP reset vector */
	REG(HOST1CFG) = DAVINCI_L3CBARAM_BASE;

	dsp_lpsc_on(1, DAVINCI_LPSC_GEM);
	REG(PSC0_MDCTL + (15 * 4)) |= 0x100;
}

int misc_init_r(void)
{
	uint8_t tmp[20], addr[10];
	unsigned int enables = 0;
	const char *bootfpga;

	/* try and read our configuration block */
	get_factory_config_block();
	get_config_block();

	printf ("ARM    Clock : %d Hz\n", clk_get(DAVINCI_ARM_CLKID));
	printf ("DDR    Clock : %d Hz\n", clk_get(DAVINCI_DDR_CLKID)/2);
	printf ("EMIFA  CLock : %d Hz\n", clk_get(DAVINCI_EMIFA_CLKID));
	printf ("DSP    Clock : %d Hz\n", clk_get(DAVINCI_DSP_CLKID));
	printf ("ASYNC3 Clock : %d Hz\n", clk_get(ASYNC3));
	printf ("Enet  config : %d\n",config_block.Peripherals.ENETConfig.EnetConfig);
	printf ("MMC 0 Enable : %d\n",config_block.Peripherals.MMCConfig[0].Enable);

	switch (config_block.Peripherals.ENETConfig.EnetConfig)
	{
	case ENET_CONFIG_MII:
		/**
		 ******** THE CODE IN BETWEEN THE *** BLOCK IS VALID FOR THE INDUSTRIAL I/O CARD *****************
		 * reset the phy. Make sure all the phy address select pins are tri-stated, toggle the reset line
		 * to the phy and then set the pins up to be MII pins.
		 */
		if (davinci_configure_pin_mux(emac_mii_reset_pins, ARRAY_SIZE(emac_mii_reset_pins)) != 0)
		{
			return 1;
		}
		printf("Resetting ethernet phy\n");

		/* disable RXD[0-3], MII_COL, and GPIO5[15] pullup/pulldown settings
		 * Group 8, 9, 5, and 19.  This allows on board pull-up / down resistors
		 * to work correctly.
		 */
		enables = readl((int*)DAVINCI_PUPD_ENA);
		enables &= ~(1 << 8);
		enables &= ~(1 << 9);
		enables &= ~(1 << 5);
		enables &= ~(1 << 19);
		writel(enables, (int*)DAVINCI_PUPD_ENA);

		/* set RDWR pin for write mode */
		/* This code assumes MII device RESET is active low, connected to
		 * pin GPIO5[15] on L138
		 */
		davinci_gpio_bank45->dir &= ~(1 << (31)); /* RDWR = 0 */
		davinci_gpio_bank45->clr_data |= (1 << 31); /* RESET = 0 */
		udelay(100);
		davinci_gpio_bank45->dir |= (1 << (31)); /* set pin back to input (release reset pulled up on board)*/
		/*
		 ******** THE CODE ABOVE IS VALID FOR THE INDUSTRIAL I/O CARD, IT MAY NOT BE VALID FOR YOUR BOARD *****
		 */

		if (davinci_configure_pin_mux(emac_mii_pins, ARRAY_SIZE(emac_mii_pins)) != 0)
		{
			return 1;
		}
		/* set cfgchip3 to selct MII */
		REG(CFGCHIP3) &= ~(1 << 8);
#ifdef CONFIG_DRIVER_TI_EMAC_USE_RMII
		using_rmii = 0;
#endif
		break;
	case ENET_CONFIG_RMII:
		if (factory_config_block.FpgaType == FPGATYPE_NONE) 
		{
			/**
			 ******** THE CODE IN BETWEEN THE *** BLOCK IS VALID FOR ONE SPECIFIC INSTANCE OF HOST CARD ****
			 * reset the phy. Make sure all the phy address select pins are tri-stated, toggle the reset line
			 * to the phy and then set the pins up to be RMII pins.
			 */
			if (davinci_configure_pin_mux(emac_rmii_reset_pins, ARRAY_SIZE(emac_rmii_reset_pins)) != 0)
			{
				return 1;
			}
			printf("Resetting MII ethernet phy\n");

			/* disable RMII_CRS_DV (group 26), RMII_RXER (group 26), RMII_RXD0 (group 26), RMII_RXD1 (group 26)
			 * pullup/pulldown settings.  This allows on board pull-up / down resistors
			 * to work correctly for bootstrapping the PHY, and we need to avoid them being applied during
			 * runtime.
			 */
			enables = readl((int*)DAVINCI_PUPD_ENA);
			enables &= ~(1 << 26);
			writel(enables, (int*)DAVINCI_PUPD_ENA);

			/* set RDWR pin for write mode */
			/* This code assumes RMII device RESET is active low, connected to
			 * pin GPIO6[10] on device
			 */
			davinci_gpio_bank67->dir &= ~(1 << 10); /* RDWR = 0 */
			davinci_gpio_bank67->clr_data |= (1 << 10); /* RESET = 0 */
			udelay(100);
			davinci_gpio_bank67->set_data |= (1 << 10); /* RESET = 1 */
			udelay(10);
			/* leave I/O pin driving... there may be a pulldown on the board... */

			/*
			 ******** THE CODE ABOVE IS VALID FOR ONE SPECIFIC INSTANCES OF HOST CARD, YOUR MILEAGE MAY VARY ***
			 */
		}

		if (davinci_configure_pin_mux(emac_rmii_pins, ARRAY_SIZE(emac_rmii_pins)) != 0)
		{
			return 1;
		}
		REG(CFGCHIP3) |= (1 << 8);
#ifdef CONFIG_DRIVER_TI_EMAC_USE_RMII
		using_rmii = 1;
#endif
		break;
	}

	// [fabian] OPTIMIZATION: requesting eth is slow. skip if ethaddr is set
	if (getenv("ethaddr") == NULL) {

		
		/* Set Ethernet MAC address from I2C EEPROM */
		memcpy(addr,factory_config_block.MACADDR, 6);

		printf("ethaddr: %s\n", (char *)tmp);
		
		if(is_valid_ether_addr(addr)) {
			sprintf((char *)tmp, "%02x:%02x:%02x:%02x:%02x:%02x", addr[0],
				addr[1], addr[2], addr[3], addr[4], addr[5]);
			setenv("ethaddr", (char *)tmp);
		}
	}
/*
		if(is_multicast_ether_addr(addr) || is_zero_ether_addr(addr)) {
			printf("Invalid MAC address read.\n");
			return -EINVAL;
		}
		sprintf((char *)tmp, "%02x:%02x:%02x:%02x:%02x:%02x", addr[0],
				addr[1], addr[2], addr[3], addr[4], addr[5]);

		setenv("ethaddr", (char *)tmp);
	}
*/

	/*
	    If there is an environment variable named "bootfpga", run the commands
	    in that variable. These commands should load the fpga as needed.
	*/
	if ((bootfpga = getenv("bootfpga")) != NULL )
	{
	    if( run_command( bootfpga, 0 ) == -1 )
		return 1;
	}

	/* 
	 * If the MMC is enabled, turn the pins on
         */
	if (config_block.Peripherals.MMCConfig[0].Enable)
	{
        	if (davinci_configure_pin_mux(mmc0_pins, ARRAY_SIZE(mmc0_pins)) != 0)
                	return 1;
	}

	dspwake();	

	return (0);
}

int board_nand_init(struct nand_chip *nand)
{
	static int numtries = 0;
	unsigned int temp;

	if (numtries++)
	{
		nand->options |= NAND_BUSWIDTH_16;
		temp = emif_regs->AB2CR;
		temp |= 1; /* set to 16 bit data width */
		emif_regs->AB2CR = temp;
	}	

	davinci_nand_init(nand);

	return 0;
}

static struct davinci_mmc mmc_sd0 = {
        .reg_base = (struct davinci_mmc_regs *)DAVINCI_MMC_SD0_BASE,
        .host_caps = MMC_MODE_4BIT,     /* DA850 supports only 4-bit SD/MMC */
        .voltages = MMC_VDD_32_33 | MMC_VDD_33_34,
        .version = MMC_CTLR_VERSION_2,
};

int board_mmc_init(bd_t *bis)
{
// [fabian]:
#ifdef CONFIG_DAVINCI_MMC
        mmc_sd0.input_clk = clk_get(DAVINCI_MMCSD_CLKID);

        /* Add slot-0 to mmc subsystem */
        return davinci_mmc_init(bis, &mmc_sd0);
#else
	return 0;
#endif
}

