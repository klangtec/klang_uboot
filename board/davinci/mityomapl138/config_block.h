/**
 * If you modify this file, you will need to copy this file over to the
 * kernel build area, or somehow make a link between the two...
 */
#ifndef CONFIG_BLOCK_H_
#define CONFIG_BLOCK_H_

#define CONFIG_MAGIC_WORD 0x00BD0138
#define CONFIG_VERSION 0x00010000

#define ENET_CONFIG_NONE 1
#define ENET_CONFIG_MII  2
#define ENET_CONFIG_RMII 3

#define CONFIG_I2C_MAGIC_WORD 0x012C0138
#define CONFIG_I2C_VERSION    0x00010001

/**
 * Peripherals Version History
 * 1.00 Baseline
 * 1.01 Added McASP Configuration
 * 1.02 Added ethernet phy mask
 * 1.03 Added MMC Configuration
 */
#define PERIPHERALS_VERSION   0x00010003

#ifndef  CONFIG_MITYDSP_ENV_SIZE
#define CONFIG_MITYDSP_ENV_SIZE (64 << 10)
#endif

#define FPGATYPE_NONE       0
#define FPGATYPE_XC6SLX9    1
#define FPGATYPE_XC6SLX16   2
#define FPGATYPE_XC6SLX25   3
#define FPGATYPE_XC6SLX45   4
#define FPGATYPE_UNKNOWN    10000

struct I2CFactoryConfig {
	u32               ConfigMagicWord;  /** CONFIG_I2C_MAGIC_WORD */
	u32               ConfigVersion;    /** CONFIG_I2C_VERSION */
	u8                MACADDR[6];       /** mac address assigned to part */
	u32               FpgaType;         /** fpga installed, see above */
	u32               Spare;            /** Not Used */
	u32               SerialNumber;     /** serial number assigned to part */
	char              PartNumber[32];   /** board part number, human readable text, NULL terminated */
};

struct UARTConfig {
	u8           Enable;            /** when non-zero enable the Tx/Rx and the device */
	u8           IsConsole;         /** when non-zero  configure as the console (u-boot && kernel), first one wins */
	u8           EnableHWFlowCtrl;  /** when non-zero  configured CTS/RTS pins and enable */
	u32          Baud;              /** default baud rate */
};

struct SPIConfig {
	u8           Enable;         /** when non-zero, configure CLK, SIMO, SOMI pins and the device */
	u8           CLKOut;         /** when non-zero, drive the CLK */
	u8           CSEnable[8];    /** when non-zero, configure the associated CS index as a CS output */
	u8           ENAEnable;      /** when non-zero, configure the ENA pin for SPI function */
	u32          CLKRate;        /** default clock rate */
	u8           Spare[8];
};

struct LCDConfig {
	u8           Enable;
	u8           PanelName[32];
};

struct ENETConfig {
	u32          EnetConfig;
	u8           MACAddr[6];
	u32          PHYMask;
	u8           Spare[8];
};

#define MCASP_PINMODE_INACTIVE 0
#define MCASP_PINMODE_TX       1
#define MCASP_PINMODE_RX       2

struct MCASPConfig {
	u8           Enable;
	u8           Mode;
	u8           PinMode[16];
	u8	     Spare[16];
};

struct gpio_id {
	u8	     Bank; /* gpio bank for MMCSD pin */
	u8	     Pin;  /* gpio pin offset inside bank for MMCSD pin */
};

struct MMCConfig {
	u8		Enable;
	struct gpio_id	WriteProtect; /* leave at zero if not used */
	struct gpio_id	CardDetect; /* leave at zero if not used */
	u8		Spare[4];
};

/**
 * struct tag_peripherals is passed in via kernel ATAG_PERIPHERALS
 */
struct tag_peripherals {
	u32                Version; /** == PERIPHERALS_VERSION */
	u8                 Manufacturer[64]; /** null terminated string indicating manufacturer */
	struct ENETConfig  ENETConfig;        /** Enable on-board ethernet */
	struct UARTConfig  UARTConfig[3];     /** default UART 0,1,2 Configuration */
	struct SPIConfig   SPIConfig[2];
	struct LCDConfig   LCDConfig;
	struct MCASPConfig MCASPConfig;
	struct MMCConfig   MMCConfig[2];
	// TODO - add arguments for pinmux settings here...
};

/**
 * This structure can only be grown.  You cannot make it smaller...
 */
struct MityDSPL138Config {
	u32               ConfigMagicWord;   /** == CONFIG_MAGIC_WORD */
	u32               ConfigVersion;     /** version of the configuration block */
	u32               ConfigSizeBytes;   /** configuration size, in bytes */
	struct tag_peripherals Peripherals;
};

struct MityDSPL138ConfigBlock {
	union {
		struct MityDSPL138Config config;
		u8                       space[CONFIG_MITYDSP_ENV_SIZE-sizeof(int)];
	} Data;
	unsigned int    CheckSum; /** summed bytes of ConfigSizeBytes */
};

extern struct MityDSPL138Config config_block;
extern struct I2CFactoryConfig  factory_config_block;
extern int get_config_block(void);
extern int get_factory_config_block(void);

#endif
