/*Define TI DSP COFF (.out) file format structure*/
#ifndef   _COFF_H
#define   _COFF_H

#define HEADER_SIZE  22
#define OPTIONAL_HEADER_SIZE  28

#define SECTION_HEADER_SIZE  48

//define of Header flag
//Relocation information was stripped from the file.
#define HEADER_FLAG_RELFLG  0x001

//The file is relocatable (it contains no unresolved external references).
#define HEADER_FLAG_EXEC  0x002

//Local symbols were stripped from the file.
#define HEADER_FLAG_LSYMS  0x008

//The target is a little-endian device.
#define HEADER_FLAG_LITTLE  0x100

//The target is a big-endian device.
#define HEADER_FLAG_BIG  0x200

//define of section type
//Regular section (allocated, relocated, loaded)
#define SECTION_TYPE_REG  0x0000000

//Dummy section (relocated, not allocated, not loaded)
#define SECTION_TYPE_DSECT  0x0000001

//Noload section (allocated, relocated, not loaded)
#define SECTION_TYPE_NOLOAD  0x0000002

/*Copy section (relocated, loaded, but not allocated;
relocation entries are processed normally)*/
#define SECTION_TYPE_COPY  0x0000010

//Section contains executable code
#define SECTION_TYPE_TEXT  0x0000020

//Section contains initialized data
#define SECTION_TYPE_DATA  0x0000040

//Section contains uninitialized data
#define SECTION_TYPE_BSS  0x0000080

//Alignment used as a blocking factor
#define SECTION_TYPE_BLOCK  0x0001000

//Section should pass through uncxhanged
#define SECTION_TYPE_PASS  0x0002000

//Section requires conditional linking
#define SECTION_TYPE_CLINK 00004000

//Section contains vector table
#define SECTION_TYPE_VECTOR  0x0008000

//Section has been padded
#define SECTION_TYPE_PADDED  0x0010000

typedef struct _tCoffHeader
{
	unsigned short usVersionID;   //indicates version of COFF file structure
	unsigned short usSectionNumber;   //Number of section headers
	int iTimeStamp;   //indicates when the file was created
	unsigned int uiSymbolPointer;   //contains the symbol table's starting address
	unsigned int uiSymbolEntryNumber;   //Number of entries in the symbol table
	//This field is either 0 or 28; if it is 0, there is no optional file header.
	unsigned short uiOptionalHeaderBytes;  //Number of bytes in the optional header
	unsigned short uiFlags;
	//magic number (0099h) indicates the file can be executed in a C6000 system
	unsigned short uiTargetID;
}TCoffHeader;

typedef struct _tCoffOptionalHeader
{
	//Optional file header magic number (0108h) indicates C6000
	unsigned short usOptionalHeaderID;
	unsigned short usVersionStamp;
	unsigned int uiTextSize;   //Integer Size (in bytes) of .text section
	unsigned  int uiDataSize;   //Integer Size (in bytes) of .data section
	unsigned  int uiBssSize;   //Integer Size (in bytes) of .bss section
	unsigned  int uiEntryPoint;
	unsigned int uiTextAddress;   //Integer Beginning address of .text section
	unsigned  int uiDataAddress; //Integer Beginning address of .data section
}TCoffOptionalHeader;

typedef struct _tSectionHeader
{
	union
	{
		char sName[8];  //An 8-character section name padded with nulls
		//A pointer into the string table if the symbol name >8bytes
		unsigned int uiPointer[2];
	}SectionName;
	unsigned int uiPysicalAddress;  //Section's physical address (Run Address)
	unsigned int uiVirtalAddress;   //Section's virtual address (Load Address)
	unsigned int uiSectionSize;   //Section size in bytes
	unsigned int uiRawDataPointer;   //File pointer to raw data
	unsigned int uiRelocationEntryPointer;  //File pointer to relocation entries
	unsigned int uiReserved0;
	unsigned int uiRelocationEntryNumber;  //Number of relocation entries
	unsigned int uiReserved1;
	/*Type of the section*/
	unsigned int uiFlags;
	unsigned short usReserved;
	unsigned short usMemoryPageNumber;
}TSectionHeader;

#endif