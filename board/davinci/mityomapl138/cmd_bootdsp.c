/*
 * Copyright (c) 2010 Critical Link, LLC <michael.williamson@criticallink.com>
 *
 * There is some code in this module that is used from Texas Instruments
 * DSPLINK framework, which is covered under GPL version 2 and includes
 * the following copyright notice:
 *
 * Copyright (C) 2002-2009, Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation version 2.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * ----------------------------------------------------------------------------
 */
#include "coff.h"
#include <common.h>
#include <command.h>
#include <linux/ctype.h>
#include <asm/arch/hardware.h>

void cache_flush(void);

int do_bootdsp (cmd_tbl_t *cmdtp, int flag, int argc, char *argv[])
{
	TCoffHeader *coffhdr;
	TSectionHeader *secthdr, shdr;
	TCoffOptionalHeader *opthdr, ohdr;
	int i, j;
	char *in, *out;
	unsigned int addr;		/* Address of the ELF image     */
	unsigned int *resetvect = (unsigned int *)DAVINCI_L3CBARAM_BASE;
	unsigned int entryPtAddrHi  = 0x00000000u ;
	unsigned int entryPtAddrLo  = 0x00000000u ;
	unsigned int startOpCodeLo  = 0x0000002Au ;
	unsigned int startOpCodeHi  = 0x0000006Au ;
	unsigned int noOp           = 0x00400000u ;
	unsigned int branch         = 0x00000362u ;
	
	/* if the device is ARM only, return */
	if ((REG(CHIP_REV_ID_REG) & 0x3f) == 0x10) {
		printf("No DSP on this chip\n");
		return 0;
	}

	if (argc < 2)
		addr = load_addr;
	else
		addr = simple_strtoul (argv[1], NULL, 16);
		
	printf ("Loading coff data from address 0x%08x\n", addr);

	coffhdr = (TCoffHeader *)addr;
	
	/* validate coff header files */
	debug(". coffhdr->usVersionID = 0x%X\n", coffhdr->usVersionID);
	debug(". coffhdr->usSectionNumber = %d\n", coffhdr->usSectionNumber);
	debug(". coffhdr->iTimeStamp = 0x%X\n", coffhdr->iTimeStamp);
	debug(". coffhdr->uiSymbolPointer = 0x%X\n", coffhdr->uiSymbolPointer);
	debug(". coffhdr->uiSymbolEntryNumber = %d\n", coffhdr->uiSymbolEntryNumber);
	debug(". coffhdr->uiOptionalHeaderBytes = %d\n", coffhdr->uiOptionalHeaderBytes);
	debug(". coffhdr->uiFlags = 0x%X\n", coffhdr->uiFlags);
	debug(". coffhdr->uiTargetID = 0x%X\n", coffhdr->uiTargetID);	
	
	if (coffhdr->uiTargetID != 0x99) {
		printf ("## Not a DSP COFF image at address 0x%08x\n", addr);
		return 0;
	}
	
	opthdr = coffhdr->uiOptionalHeaderBytes ? 
	    (TCoffOptionalHeader *)(addr+HEADER_SIZE) : 0;
	if (!opthdr) {
		printf("No Optional Header / Entry Point found\n");
		return 0;
	}
	
	/* ARM doesn't appear to like unaligned ints, copy it for alignment */
	memcpy(&ohdr, opthdr, sizeof(ohdr));
	opthdr = &ohdr;
	
	if (opthdr) {
		debug(".. OptHeader->usOptionalHeaderID = 0x%X\n", opthdr->usOptionalHeaderID);
		debug(".. OptHeader->usVersionStamp = 0x%X\n", opthdr->usVersionStamp);
		debug(".. OptHeader->uiDataSize = %d bytes\n", opthdr->uiDataSize);
		debug(".. OptHeader->uiBssSize = %d bytes\n", opthdr->uiBssSize);
		debug(".. OptHeader->uiEntryPoint = 0x%X\n", opthdr->uiEntryPoint);
		debug(".. OptHeader->uiTextAddress = 0x%X\n", opthdr->uiTextAddress);
		debug(".. OptHeader->uiDataAddress = 0x%X\n", opthdr->uiDataAddress);
	}
	
	secthdr = (TSectionHeader *)(addr+HEADER_SIZE+
								 coffhdr->uiOptionalHeaderBytes);

	for (i = 0; i < coffhdr->usSectionNumber; i++, secthdr++) {
		memcpy(&shdr, secthdr, sizeof(shdr));
		if (!(shdr.uiFlags & (SECTION_TYPE_NOLOAD | SECTION_TYPE_DSECT | SECTION_TYPE_BSS | SECTION_TYPE_COPY) || !shdr.uiVirtalAddress)) {
			printf ("... loading %d bytes to address 0x%08x\n", 
			        shdr.uiSectionSize, shdr.uiVirtalAddress);
			in = (char*)(addr+shdr.uiRawDataPointer);
			out = (char*)(shdr.uiVirtalAddress);
			for (j = 0; j < shdr.uiSectionSize; j++) {
				*out++ = *in++;
			}
		}
	}
   	
	printf("Starting DSP at address 0x%08x\n", opthdr ? opthdr->uiEntryPoint : 0);
		
	/* shamelessly lifted from DSPLINK */
	
	/* hold reset */
	REG(PSC0_MDCTL + (15 * 4)) &= ~0x100;
	
	/* enable module (still in local reset) */
	if ((REG(PSC0_MDCTL + (15 * 4)) & 0x7) != 3) {
		int temp = REG(PSC0_MDCTL + (15 * 4));
		temp &= ~7;
		temp |= 3;
		REG(PSC0_MDCTL + (15 * 4)) = temp;
		REG(PSC0_PTCMD) = 1;
		for (temp = 0; temp < 1000; temp++) ;
		while(REG(PSC0_PTSTAT) & 1) ;
	}

	for (i = 0; i < 10000; i++) ;
	
	/* hack in some code to jump to the real entry point */
	entryPtAddrHi = opthdr->uiEntryPoint >> 16;
	entryPtAddrLo = opthdr->uiEntryPoint & 0xFFFF;
	startOpCodeHi |= (entryPtAddrHi << 7);
	startOpCodeLo |= (entryPtAddrLo << 7);
	*resetvect++ = startOpCodeLo;
	*resetvect++ = startOpCodeHi;
	*resetvect++ = branch;
	*resetvect++ = noOp;
	*resetvect++ = noOp;
	*resetvect++ = noOp;
	*resetvect++ = noOp;
	*resetvect++ = noOp;

	/* setup the DSP reset vector */
	REG(HOST1CFG) = DAVINCI_L3CBARAM_BASE;
	
	cache_flush();

	for (i = 0; i < 10000; i++) ;

	/* Release reset */
	REG(PSC0_MDCTL + (15 * 4)) |= 0x100;
	
	return 0;
}

/* ====================================================================== */
U_BOOT_CMD(
	bootdsp,      2,      0,      do_bootdsp,
	"Boot DSP from an a.out COFF image in memory",
	" [address] - load address of DSP image."
);
